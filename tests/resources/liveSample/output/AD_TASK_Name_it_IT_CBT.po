msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: \n"
"PO-Revision-Date: \n"
"Project-Id-Version: AD_TASK_Name_it_IT.po\n"
"Language-Team:  \n"
"X-Generator: com.openbravo.xmlpo.xml2po\n"
"X-Openbravo-Table: AD_TASK\n"
"X-Openbravo-Column: Name\n"
"X-Openbravo-Language: it_IT\n"
"MIME-Version: 1.0\n"

# RowIds:
# 102
# Name: Java Version [102]
# Description: Displays the version of the default Java VM [102]
# Help: The java version used by the application might be different. [102]
msgid "Java Version"
msgstr "Versione Java"

# RowIds:
# 103
# Name: Database Export [103]
# Description: Export (save) the database [103]
# Help: Run this command from the server [103]
msgid "Database Export"
msgstr "Esportazione Database"

# RowIds:
# 104
# Name: Database Transfer [104]
# Description: Transfer the database [104]
# Help: Run this command from the server [104]
msgid "Database Transfer"
msgstr "Trasferimento Database "

# RowIds:
# 800000
# Name: Merge Script [800000]
# Description:  [800000]
# Help:  [800000]
msgid "Merge Script"
msgstr "Unione dello Script"

# RowIds:
# 800001
# Name: Preinstall [800001]
# Description:  [800001]
# Help:  [800001]
msgid "Preinstall"
msgstr "Pre installazione"

# RowIds:
# 800002
# Name: Version [800002]
# Description:  [800002]
# Help:  [800002]
msgid "Version"
msgstr "Versione"

# RowIds:
# 800003
# Name: External Point of Sales [800003]
# Description:  [800003]
# Help:  [800003]
msgid "External Point of Sales"
msgstr "Punti esterni di vendita"

# RowIds:
# 800004
# Name: fdd [800004]
# Description:  [800004]
# Help:  [800004]
msgid "fdd"
msgstr "fdd"

