/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.objects.POFileObject;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueType;
import org.openbravo.xmlpo.utils.FileObjectBuilder;



public class TestFileObjectBuilder {
  
  private POFileObject poFileObj;
  private FileObjectBuilder builder;
  
  @Before
  public void setUp() throws Exception {
    builder = new FileObjectBuilder();
    
    poFileObj = new POFileObject();
    poFileObj.setTableName("AD_TEST");
    poFileObj.setLanguage("it_IT");
    
  }
  
  @After
  public void tearDown() throws Exception {
    builder = null;
    poFileObj = null;
  }
  
  @Test
  public final void testCreateFileObject() {
    Collection<Message> msgs = new ArrayList<Message>();
    poFileObj.setColumnName("Name");
    poFileObj.setColumnIndex("1");
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Name Id", "Name string", "Name"));
    poFileObj.addMessages(msgs);
    builder.setPOFileObject(poFileObj);
    builder.createFileObject();

    FileObject fileObj = builder.getFileObject();
    assertEquals("TableName not as expected.", "AD_TEST", fileObj.getTableName());
    assertEquals("Language not as expected.", "it_IT", fileObj.getLanguage());
    assertNotNull("RowObjects collection is null.", fileObj.getRowObjects());
    assertEquals("fileObject.getValueTypes not the correct size.", 1, fileObj.getValueTypes().size());
    for (Iterator<ValueType> iterator = fileObj.getValueTypes().iterator(); iterator.hasNext();) {
      ValueType vType = (ValueType) iterator.next();
      assertEquals("Name", vType.getName());
      assertEquals(1, vType.getIntValue());
    }
    assertEquals("Size of rowObjects collection not as expected.", 1, fileObj.getRowObjects().size());
    for (Iterator<RowObject> iterator = fileObj.getRowObjects().iterator(); iterator.hasNext();) {
      RowObject rowObj = (RowObject) iterator.next();
      assertEquals("5973945635C6FAB2E040A8C0846648BC", rowObj.getRowId());
    }
  }
  
  @Test
  public final void testCreateFileObjectMultipleTypes() {
    Collection<Message> msgs = new ArrayList<Message>();
    poFileObj.setColumnName("Name");
    poFileObj.setColumnIndex("1");
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Name Id", "Name string", "Name"));
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Description Id", "Description string", "Description"));
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Help Id", "Help string", "Help"));
    poFileObj.addMessages(msgs);
    builder.setPOFileObject(poFileObj);
    builder.createFileObject();

    FileObject fileObj = builder.getFileObject();
    assertEquals("TableName not as expected.", "AD_TEST", fileObj.getTableName());
    assertEquals("Language not as expected.", "it_IT", fileObj.getLanguage());
    assertNotNull("RowObjects collection is null.", fileObj.getRowObjects());
    assertEquals("fileObject.getValueTypes not the correct size.", 3, fileObj.getValueTypes().size());
    assertEquals("Size of rowObjects collection not as expected.", 1, fileObj.getRowObjects().size());
    for (Iterator<RowObject> iterator = fileObj.getRowObjects().iterator(); iterator.hasNext();) {
      RowObject rowObj = (RowObject) iterator.next();
      assertEquals("5973945635C6FAB2E040A8C0846648BC", rowObj.getRowId());
      assertEquals(3, rowObj.getValueObjects().size());
    }
  }
  
  @Test
  public final void testCreateFileObjectMultipleRowObjects() {
    Collection<Message> msgs = new ArrayList<Message>();
    poFileObj.setColumnName("Name");
    poFileObj.setColumnIndex("1");
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Name Id 5973945635C6FAB2E040A8C0846648BC", "Name string 5973945635C6FAB2E040A8C0846648BC", "Name"));
    msgs.add(createMessages("199", "Name Id 199", "Name string 199", "Name"));
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Description Id 5973945635C6FAB2E040A8C0846648BC", "Description string 5973945635C6FAB2E040A8C0846648BC", "Description"));
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Help Id 5973945635C6FAB2E040A8C0846648BC", "Help string 5973945635C6FAB2E040A8C0846648BC", "Help"));
    poFileObj.addMessages(msgs);
    builder.setPOFileObject(poFileObj);
    builder.createFileObject();

    FileObject fileObj = builder.getFileObject();
    assertEquals("TableName not as expected.", "AD_TEST", fileObj.getTableName());
    assertEquals("Language not as expected.", "it_IT", fileObj.getLanguage());
    assertNotNull("RowObjects collection is null.", fileObj.getRowObjects());
    assertEquals("fileObject.getValueTypes not the correct size.", 3, fileObj.getValueTypes().size());
    assertEquals("Size of rowObjects collection not as expected.", 2, fileObj.getRowObjects().size());
    for (Iterator<RowObject> iterator = fileObj.getRowObjects().iterator(); iterator.hasNext();) {
      RowObject rowObj = (RowObject) iterator.next();
      if (rowObj.getRowId().equals("5973945635C6FAB2E040A8C0846648BC")) {
        assertEquals("5973945635C6FAB2E040A8C0846648BC", rowObj.getRowId());
        assertEquals(3, rowObj.getValueObjects().size());
      } else if (rowObj.getRowId().equals("199")) {
        assertEquals("199", rowObj.getRowId());
        assertEquals(1, rowObj.getValueObjects().size());
      } else {
        fail("row object not as expected: " + rowObj.getRowId());
      }
    }
  }
  
  @Test
  public final void testFileObjectTrl() {
    Collection<Message> msgs = new ArrayList<Message>();
    poFileObj.setColumnName("Name");
    poFileObj.setColumnIndex("1");
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Name Id", "Name string", "Name", "Y"));
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Description Id", "Description string", "Description", "N"));
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Help Id", "Help string", "Help", "N"));
    poFileObj.addMessages(msgs);
    builder.setPOFileObject(poFileObj);
    builder.createFileObject();

    FileObject fileObj = builder.getFileObject();
    assertEquals("TableName not as expected.", "AD_TEST", fileObj.getTableName());
    assertEquals("Language not as expected.", "it_IT", fileObj.getLanguage());
    assertNotNull("RowObjects collection is null.", fileObj.getRowObjects());
    assertEquals("fileObject.getValueTypes not the correct size.", 3, fileObj.getValueTypes().size());
    assertEquals("Size of rowObjects collection not as expected.", 1, fileObj.getRowObjects().size());
    for (Iterator<RowObject> iterator = fileObj.getRowObjects().iterator(); iterator.hasNext();) {
      RowObject rowObj = (RowObject) iterator.next();
      assertEquals("5973945635C6FAB2E040A8C0846648BC", rowObj.getRowId());
      assertEquals("", true, rowObj.getTranslation());
      assertEquals(3, rowObj.getValueObjects().size());
    }
  }
  
  @Test
  public final void testFileObjectTrlOrdering() {
    Collection<Message> msgs = new ArrayList<Message>();
    poFileObj.setColumnName("Name");
    poFileObj.setColumnIndex("1");
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Name Id", "Name string", "Name", "N"));
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Description Id", "Description string", "Description", "Y"));
    msgs.add(createMessages("5973945635C6FAB2E040A8C0846648BC", "Help Id", "Help string", "Help", "N"));
    poFileObj.addMessages(msgs);
    builder.setPOFileObject(poFileObj);
    builder.createFileObject();

    FileObject fileObj = builder.getFileObject();
    assertEquals("TableName not as expected.", "AD_TEST", fileObj.getTableName());
    assertEquals("Language not as expected.", "it_IT", fileObj.getLanguage());
    assertNotNull("RowObjects collection is null.", fileObj.getRowObjects());
    assertEquals("fileObject.getValueTypes not the correct size.", 3, fileObj.getValueTypes().size());
    assertEquals("Size of rowObjects collection not as expected.", 1, fileObj.getRowObjects().size());
    for (Iterator<RowObject> iterator = fileObj.getRowObjects().iterator(); iterator.hasNext();) {
      RowObject rowObj = (RowObject) iterator.next();
      assertEquals("5973945635C6FAB2E040A8C0846648BC", rowObj.getRowId());
      assertEquals("", true, rowObj.getTranslation());
      assertEquals(3, rowObj.getValueObjects().size());
    }
  }
  
  private Message createMessages(String rowId, String msgId, String msgStr, String columnName) {
    Message msg = new Message();
    msg.setRowIdRow(rowId);
    msg.setMessageId(msgId);
    msg.setMessageString(msgStr);
    msg.setColumn(columnName);
    return msg;
  }

  private Message createMessages(String rowId, String msgId, String msgStr, String columnName, String msgTrl) {
    Message msg = new Message();
    msg.setRowIdRow(rowId);
    msg.setMessageId(msgId);
    msg.setMessageString(msgStr);
    msg.setColumn(columnName);
    msg.setRowTrl(msgTrl);
    return msg;
  }
}
