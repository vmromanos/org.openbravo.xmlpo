/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import static org.junit.Assert.*;

import java.io.File;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.exceptions.InvalidMergeException;
import org.openbravo.xmlpo.objects.POFileObject;
import org.openbravo.xmlpo.utils.MessageParser;
import org.openbravo.xmlpo.utils.POFileParser;
import org.openbravo.xmlpo.utils.POFileParser.ParseType;


public class TestPOFileParser {
  
  private static final Logger log4j = Logger.getLogger(TestPOFileParser.class);

  private POFileParser parser;
  private MessageParser msgParser;
  private String testFile = "tests/resources/merge/first/testGetFirstFile.po";
  private String testFileMatching = "tests/resources/merge/second/testGetSecondFile.po";
  private String testFileNotMatching = "tests/resources/merge/testFileNotMatchingHeader.po";
  
  @Before
  public void setUp() throws Exception {
    msgParser = new MessageParser();
    parser = new POFileParser(msgParser);
  }

  @After
  public void tearDown() throws Exception {
    msgParser = null;
    parser = null; 
  }
  
  @Test
  public final void testPOFileParser() {
    POFileParser testParser = new POFileParser(msgParser);
    assertNotNull(testParser.getPOFileObject());
    
    POFileParser test2Parser = new POFileParser();
    assertNotNull(test2Parser.getPOFileObject());
  }
  
  @Test
  public final void testSetFile() {
    parser.setFile(new File(testFile));
    assertEquals("testGetFirstFile.po", parser.getFile().getName());
  }
  
  @Test
  public final void testParseFile() {
    // method that calls the readFile method
    parser.setFile(new File(testFile));
    parser.setParseType(ParseType.MERGE);
    try {
      parser.parseFile();
    } catch (InvalidMergeException e) {
      fail(e.getMessage());
    }
    POFileObject fileObj = parser.getPOFileObject();
    assertEquals("AD_TASK", fileObj.getTableName());
    assertEquals("Name", fileObj.getColumnName());
    assertEquals("it_IT", fileObj.getLanguage());
    assertEquals("Version not as expected.", "2.40", fileObj.getVersion());
    assertEquals("AD_TASK_Name_it_IT.po", fileObj.getFileName());
    
  }
  
  @Test
  public final void testGetMessageParser() {
    POFileParser newParser = new POFileParser();
    newParser.setMessageParser(new MessageParser());
    assertNotNull(newParser.getMessageParser());
  }
  
  @Test
  public final void testSetMessageParser() {
    POFileParser newParser = new POFileParser();
    newParser.setMessageParser(new MessageParser());
    assertNotNull(newParser.getMessageParser());
  }
  
  @Test
  public final void testFilesMatch() {
    log4j.debug("#### Commencing testFilesMatch test case ####");
    assertTrue(parser.getFileMatchResult());
    parser.setFile(new File(testFile));
    parser.setParseType(ParseType.MERGE);
    try {
      parser.parseFile();
    } catch (InvalidMergeException e1) {
      fail(e1.getMessage());
    }
    assertTrue(parser.getFileMatchResult());
    
    // add file with matching details
    parser.setFile(new File(testFileMatching));
    parser.setParseType(ParseType.MERGE);
    try {
      parser.parseFile();
    } catch (InvalidMergeException e1) {
      fail(e1.getMessage());
    }
    assertTrue(parser.getFileMatchResult());
    
    // add file without matching details
    parser.setFile(new File(testFileNotMatching));
    try {
      parser.parseFile();
      //assertFalse(parser.getFileMatchResult());
      fail("error not caught");
    } catch (InvalidMergeException e) {
      log4j.info("#### Completed testFilesMatch test case ####");
      assertNotNull(e.getMessage());
    }
  }
  
}
