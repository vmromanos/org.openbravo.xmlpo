/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import java.io.File;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import junit.framework.TestCase;

import org.junit.Test;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;
import org.openbravo.xmlpo.xml2po.POContainer;
import org.xml.sax.SAXException;


public class TestFileHandler extends TestCase {
  
  @Test
  public void testLongValueHandling() throws Exception {
    File file = new File("tests/resources/input/AD_LONGTEST_TRL_en_US.xml");
    
    FileObject fileObj = new FileObject();
    fileObj.setContainer(new POContainer());
    FileHandler handler = new FileHandler(fileObj);
    SAXParser parser = inititeSAXParser();
    parser.parse(file, handler);
    fileObj = handler.getFileObj();
    
    assertEquals("File name not as expected: ", "AD_TASK", fileObj.getTableName());
    assertEquals("File language not as expected: ", "en_US", fileObj.getLanguage());
    assertEquals("Number of rows in file object not correct: ", 1, fileObj.getRowObjects().size());
    
    String resultOrig = "ATTENTION: ANY CHANGES here will effect the structure of your chart of accounts!\nFor changes to become effective, you must re-login and re-start the Application Server.";
    String resultValue = "ATTENTION: ANY CHANGES here will effect the structure of your chart of accounts!\nFor changes to become effective, you must re-login and re-start the Application Server.";
    for (Iterator<RowObject> it = fileObj.getRowObjects().iterator(); it.hasNext();) {
      RowObject rowObj = (RowObject) it.next();
      for (Iterator<ValueObject> iter = rowObj.getValueObjects().iterator(); iter.hasNext();) {
        ValueObject valueObj = (ValueObject) iter.next();
        assertEquals("Original Value not as Expected: ", resultOrig, valueObj.getOriginal());
        assertEquals("Value not as Expected: ", resultValue, valueObj.getValue());
      }
    }
  }
  
  @Test 
  public void testFileHandlerParsing() throws Exception {
    File file = new File("tests/resources/AD_TASK_TRL_en_US.xml");
    
    FileObject fileObj = new FileObject();
    fileObj.setContainer(new POContainer());
    FileHandler handler = new FileHandler(fileObj);
    SAXParser parser = inititeSAXParser();
    parser.parse(file, handler);
    fileObj = handler.getFileObj();
    
    assertEquals("File name not as expected: ", "AD_TASK", fileObj.getTableName());
    assertEquals("File language not as expected: ", "en_US", fileObj.getLanguage());
    assertEquals("Number of rows in file object not correct: ", 8, fileObj.getRowObjects().size());
    
    int valueCount = 0;
    String rowId = "";
    String valueName = "";
    String suffix = " Value";
    for (Iterator<RowObject> it = fileObj.getRowObjects().iterator(); it.hasNext();) {
      RowObject rowObj = (RowObject) it.next();
      rowId = rowObj.getRowId();
      for (Iterator<ValueObject> iter = rowObj.getValueObjects().iterator(); iter.hasNext();) {
        ValueObject valueObj = (ValueObject) iter.next();
        valueName = valueObj.getColumn();
        assertEquals("Original Value not as Expected: ", rowId + " " + valueName, valueObj.getOriginal());
        assertEquals("DataValue Value not as Expected: ", rowId + " " + valueName + suffix, valueObj.getValue());
        valueCount++;
      }
    }
    assertEquals("Number of value entried not correct: ", 24, valueCount);
  }
  
  @Test
  public void testFileVersionHandling() throws Exception {
    File file = new File("tests/resources/AD_VERSIONEDTASK_TRL_en_US.xml");
    
    FileObject fileObj = new FileObject();
    fileObj.setContainer(new POContainer());
    FileHandler handler = new FileHandler(fileObj);
    SAXParser parser = inititeSAXParser();
    parser.parse(file, handler);
    
    assertEquals("File name not as expected: ", "AD_VERSIONEDTASK", fileObj.getTableName());
    assertEquals("File language not as expected: ", "en_US", fileObj.getLanguage());
    assertEquals("File version number not as expected", "2.40", fileObj.getVersion());
    assertEquals("Number of rows in file object not correct: ", 8, fileObj.getRowObjects().size());
    
  }
  
  private SAXParser inititeSAXParser() throws ParserConfigurationException, SAXException {
    SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    saxParserFactory.setNamespaceAware(true);
    saxParserFactory.setValidating(true);
    SAXParser saxParser = saxParserFactory.newSAXParser();
    return saxParser;
  }
  
  @Test
  public void testFileHandlerHandlingReturnText() throws Exception {
    File file = new File("tests/resources/XML2PO/input/AD_TASK_TRL_en_US.xml");
    
    FileObject fileObj = new FileObject();
    fileObj.setContainer(new POContainer());
    FileHandler handler = new FileHandler(fileObj);
    SAXParser parser = inititeSAXParser();
    parser.parse(file, handler);
    
    assertEquals("File name not as expected: ", "AD_TASK", fileObj.getTableName());
    assertEquals("File language not as expected: ", "en_US", fileObj.getLanguage());
    assertEquals("File version number not as expected", "2.40", fileObj.getVersion());
    assertEquals("Number of rows in file object not correct: ", 3, fileObj.getRowObjects().size());
    
    int valueCount = 0;
    for (Iterator<RowObject> it = fileObj.getRowObjects().iterator(); it.hasNext();) {
      RowObject rowObj = (RowObject) it.next();
      for (Iterator<ValueObject> iter = rowObj.getValueObjects().iterator(); iter.hasNext();) {
        ValueObject valueObj = (ValueObject) iter.next();
        assertEquals("Original Value not as Expected: ", "\\n", valueObj.getOriginal());
        if (rowObj.getRowId().equals("102")) {
          assertEquals("DataValue Value not as Expected: ", "\\n", valueObj.getValue());
        } else if (rowObj.getRowId().equals("103")) {
          assertEquals("DataValue Value not as Expected: ", "\n", valueObj.getValue());
        } else if (rowObj.getRowId().equals("104")) {
          assertEquals("DataValue Value not as Expected: ", "CDATA[\\n]", valueObj.getValue());
        }
        valueCount++;
      }
    }
    assertEquals("Number of value entried not correct: ", 3, valueCount);
  }
}
