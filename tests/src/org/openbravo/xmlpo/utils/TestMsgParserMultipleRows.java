package org.openbravo.xmlpo.utils;


import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.utils.MessageParser;


public class TestMsgParserMultipleRows {
  
  private static final Logger log4j = Logger.getLogger(TestMessageParser.class);

  private MessageParser parser;
  
  @Before
  public void setUp() throws Exception {
    parser = new MessageParser();
  }
  
  @After
  public void tearDown() throws Exception {
    parser = null;
  }
  
  @Test
  public final void testParseLineMsgId() {
    String[] msgLines = createMultiRowMessage();
    for (int i = 0; i < msgLines.length; i++) {
      parser.parseLine(msgLines[i]);
    }
    assertEquals("Number of msgs not as expected.", 4, parser.getMessages().size());
    Collection<Message> msgs = parser.getMessages().values();
    for (Iterator<Message> iterator = msgs.iterator(); iterator.hasNext();) {
      Message message = (Message) iterator.next();
      log4j.debug("RowId: " + message.getRowIdRow());
      log4j.debug("MsgId: " + message.getMessageId());
      log4j.debug("MsgStr: " + message.getMessageString());
      
    }
  }
  
  private String[] createMultiRowMessage() {
    String[] strings = new String[] {
        "\n",
        "# RowIds:\n",
        "# 112,123,256,353\n",
        "# Name: Java Version []\n",
        "# Description: Displays the version of the default Java VM []\n",
        "# Help: The java version used by the application might be different. []\n",
        "msgid \"Java Version\"\n",
        "msgstr \"Versione Java\"\n",
        "\n"};
    return strings;
  }
  
}
