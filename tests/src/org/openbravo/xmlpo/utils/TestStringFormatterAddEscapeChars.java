/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;

public class TestStringFormatterAddEscapeChars {
	
	private static final Logger log4j = Logger.getLogger(TestStringFormatterAddEscapeChars.class);

	@Test
	public void testSpecialCharacterListAddEscapeChars() {
		String testString = "This is a \\\"test\" if the \\&quot;character&quot; list";
		log4j.debug("testString: " + testString);
		String testResult = "This is a \\\\\\\"test\\\" if the \\\\&quot;character\\&quot; list";
		log4j.debug("expected result: " + testResult);
		
		assertEquals(testResult, testResult);
	}
	
	@Test
	public void testEscapedQuotesAddEscapeChars() {
		String testString = "This is a \\\"test\\\" if the \\\"character\\\" list";
		log4j.debug("testString: " + testString);
		String testResult = "This is a \\\\\\\"test\\\\\\\" if the \\\\\\\"character\\\\\\\" list";
		log4j.debug("expected result: " + testResult);
		
		String result = StringFormatter.stringEscapeFormat(testString, true);
		assertEquals(testResult, result);
	}
	
	@Test
	public void testUnescapedQuotesAddEscapeChars() {
		String testString = "This is a \"test\" if the \"character\" list";
		log4j.debug("testString: " + testString);
		String testResult = "This is a \\\"test\\\" if the \\\"character\\\" list";
		log4j.debug("expected result: " + testResult);
		
		String result = StringFormatter.stringEscapeFormat(testString, true);
		assertEquals(testResult, result);
	}
	
	@Test
	public void testMixedQuotesAddEscapeChars() {
		String testString = "This is a \"test\\\" if the \\\"character\" list";
		log4j.debug("testString: " + testString);
		String testResult = "This is a \\\"test\\\\\\\" if the \\\\\\\"character\\\" list";
		log4j.debug("expected result: " + testResult);
		
		String result = StringFormatter.stringEscapeFormat(testString, true);
		assertEquals(testResult, result);
	}
	
	@Test
	public void testMixedQuotesBoundariesAddEscapeChars() {
		String testString = "\\\"This is a test\" if the \\\"character list\"";
		log4j.debug("testString: " + testString);
		String testResult = "\\\\\\\"This is a test\\\" if the \\\\\\\"character list\\\"";
		log4j.debug("expected result: " + testResult);
		
		String result = StringFormatter.stringEscapeFormat(testString, true);
		assertEquals(testResult, result);
	}
}
