/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestStringFormatterRemoveEscapeChars {

	@Test
	public void testSpecialCharacterListRemoveEscapeChars() {
		String testResult = "This is a \\\"test\" if the \\&quot;character&quot; list";
		assertEquals(testResult, testResult);
	}
	
	@Test
	public void testEscapedQuotesRemoveEscapeChars() {
		String testResult = "This is a \\\"test\\\" if the \\\"character\\\" list";
		String testString = "This is a \\\\\\\"test\\\\\\\" if the \\\\\\\"character\\\\\\\" list";
		
		String result = StringFormatter.stringEscapeFormat(testString, false);
		assertEquals(testResult, result);
	}
	
	@Test
	public void testUnescapedQuotesRemoveEscapeChars() {
		String testResult = "This is a \"test\" if the \"character\" list";
		String testString = "This is a \\\"test\\\" if the \\\"character\\\" list";
		
		String result = StringFormatter.stringEscapeFormat(testString, false);
		assertEquals(testResult, result);
	}
	
	@Test
	public void testMixedQuotesRemoveEscapeChars() {
		String testResult = "This is a \"test\\\" if the \\\"character\" list";
		String testString = "This is a \\\"test\\\\\\\" if the \\\\\\\"character\\\" list";
		
		String result = StringFormatter.stringEscapeFormat(testString, false);
		assertEquals(testResult, result);
	}
	
	@Test
	public void testMixedQuotesBoundariesRemoveEscapeChars() {
		String testResult = "\\\"This is a test\" if the \\\"character list\"";
		String testString = "\\\\\\\"This is a test\\\" if the \\\\\\\"character list\\\"";
		
		String result = StringFormatter.stringEscapeFormat(testString, false);
		assertEquals(testResult, result);
	}
}
