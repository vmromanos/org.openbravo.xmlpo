/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.utils.MessageParser;

public class TestMessageParser {
  
  private static final Logger log4j = Logger.getLogger(TestMessageParser.class);

  private MessageParser parser;
  private String testMsgId = "msgid \"Test id\"\n";
  private String testMsgStr = "msgstr \"Test str\"\n";
  private String messageStartString = "# RowIds:\n";
  private String commentRowId = "# 102\n";
  private String commentLineName = "# Name: Java Version [102]\n";
  private String commentLineDescription = "# Description: Displays the version of the default Java VM [102]\n";
  
  @Before
  public void setUp() throws Exception {
    parser = new MessageParser();
  }
  
  @After
  public void tearDown() throws Exception {
    parser = null;
  }
  
  @Test
  public final void testMessageParser() {
    assertNotNull(parser.getMessage());
  }
  
  @Test
  public final void testParseLineMsgId() {
    parser.parseLine(messageStartString);
    parser.parseLine(commentRowId);
    parser.parseLine(testMsgId);
    assertEquals("Test id", parser.getMessage().getMessageId());
  }
  
  @Test
  public final void testParseLineMsgIdEmpty() {
    parser.parseLine(messageStartString);
    parser.parseLine(commentRowId);
    parser.parseLine("msgid \"\"\n");
    assertEquals("", parser.getMessage().getMessageId());
  }
  
  @Test
  public final void testParseLineMsgStr() {
    parser.parseLine(messageStartString);
    parser.parseLine(commentRowId);
    parser.parseLine(testMsgStr);
    assertEquals("Test str", parser.getMessage().getMessageString());
  }
  
  @Test
  public final void testParseLineMsgStrEmpty() {
    parser.parseLine(messageStartString);
    parser.parseLine(commentRowId);
    parser.parseLine("msgstr \"\"\n");
    assertEquals("testParseLineMsgStrEmpty() msgString not returned empty.", "", parser.getMessage().getMessageString());
  }
  
  @Test
  public final void testParseLineStartElement() {
    parser.parseLine(messageStartString);
    parser.parseLine(commentRowId);
    parser.parseLine(testMsgStr);
    assertEquals("Test str", parser.getMessage().getMessageString());
    parser.parseLine(commentLineName);
    assertEquals(commentLineName + "\n", parser.getMessage().getCommentString());
    parser.parseLine(messageStartString);
    assertNull(parser.getMessage().getMessageString());
    assertEquals(null, parser.getMessage().getCommentString());
  }
  
  @Test
  public final void testParseCommentLineName() {
    parser.parseLine(messageStartString);
    assertEquals(null, parser.getMessage().getCommentString());
    parser.parseLine(commentRowId);
    assertEquals(null, parser.getMessage().getCommentString());
    parser.parseLine(commentLineName);
    assertEquals(commentLineName + "\n", parser.getMessage().getCommentString());
    parser.parseLine(commentLineDescription);
    assertEquals(commentLineName + "\n" + commentLineDescription + "\n", parser.getMessage().getCommentString());
  }
  
  @Test
  public final void testParseMessageRowId() {
    parser.parseLine(messageStartString);
    assertEquals(null, parser.getMessage().getCommentString());
    parser.parseLine(commentRowId);
    assertEquals(null, parser.getMessage().getCommentString());
    assertEquals("102", parser.getMessage().getRowIdRow());
  }
  
  @Test
  public final void testGetMessage() {
    assertNotNull(parser.getMessage());
  }
  
  @Test
  public final void testMultipleLineMsgStr() {
    parser.parseLine(messageStartString);
    assertEquals(null, parser.getMessage().getCommentString());
    parser.parseLine(commentRowId);
    assertEquals(null, parser.getMessage().getCommentString());
    assertEquals("102", parser.getMessage().getRowIdRow());
    parser.parseLine("msgid \"\"\n");
    assertEquals("", parser.getMessage().getMessageId());
    parser.parseLine("\"Test Id\n\"\n");
    assertEquals("Test Id\n", parser.getMessage().getMessageId());
    parser.parseLine("\"testing multi line id's\"");
    assertEquals("Test Id\ntesting multi line id's", parser.getMessage().getMessageId());
    parser.parseLine(testMsgStr);
    assertEquals("Test Id\ntesting multi line id's", parser.getMessage().getMessageId());
    assertEquals("Test str", parser.getMessage().getMessageString());
    parser.parseLine("\".Another Line.\n\"\n");
    assertEquals("Test Id\ntesting multi line id's", parser.getMessage().getMessageId());
    assertEquals("Test str\n.Another Line.\n", parser.getMessage().getMessageString());
  }
  
  @Test
  public final void testMultipleLineMsgStrStartingWithMsgIdRow() {
    parser.parseLine(messageStartString);
    assertEquals(null, parser.getMessage().getCommentString());
    parser.parseLine(commentRowId);
    assertEquals(null, parser.getMessage().getCommentString());
    assertEquals("102", parser.getMessage().getRowIdRow());
    parser.parseLine("msgid \"Line Starts Here\"\n");
    assertEquals("Line Starts Here", parser.getMessage().getMessageId());
    parser.parseLine("\"Test Id\n\"\n");
    assertEquals("Line Starts Here\nTest Id\n", parser.getMessage().getMessageId());
    parser.parseLine("\"testing multi line id's\"");
    assertEquals("Line Starts Here\nTest Id\ntesting multi line id's", parser.getMessage().getMessageId());
    parser.parseLine(testMsgStr);
    assertEquals("Line Starts Here\nTest Id\ntesting multi line id's", parser.getMessage().getMessageId());
    assertEquals("Test str", parser.getMessage().getMessageString());
    parser.parseLine("\".Another Line.\n\"\n");
    assertEquals("Line Starts Here\nTest Id\ntesting multi line id's", parser.getMessage().getMessageId());
    assertEquals("Test str\n.Another Line.\n", parser.getMessage().getMessageString());
  }
  
  @Test
  public final void testRowTrlParsing() {
    parser.parseLine("# RowTrl: Y");
    assertEquals(true, parser.getMessage().getRowTrl());
    parser.parseLine("# RowTrl: N");
    assertEquals(false, parser.getMessage().getRowTrl());
    parser.parseLine("# RowTrl: n");
    assertEquals(false, parser.getMessage().getRowTrl());
    parser.parseLine("# RowTrl: y");
    assertEquals(true, parser.getMessage().getRowTrl());
  }
  
  @Test
  public final void testRowTrlSetToFalseForEmptyMsgString() {
	parser = new MessageParser();
	parser.parseLine(messageStartString);
	parser.parseLine(commentRowId);
	parser.parseLine("# RowTrl: Y");
	parser.parseLine(testMsgId);
	parser.parseLine("msgstr \"\"\n");
	parser.parseLine("\n");
	assertEquals(1, parser.getMessages().size());
    assertEquals("Test id", parser.getMessages().get("102").getMessageId());
    assertEquals("", parser.getMessages().get("102").getMessageString());
	assertEquals("102", parser.getMessages().get("102").getRowIdRow());
	assertEquals(false, parser.getMessages().get("102").getRowTrl());
  }
  
  @Test
  public final void testRowTrlParsingWithStringVariable() {
	parser = new MessageParser();
	parser.parseLine(messageStartString);
	parser.parseLine(commentRowId);
	parser.parseLine("# RowTrl: Y");
	parser.parseLine(testMsgId);
	parser.parseLine(testMsgStr);
	parser.parseLine("\n");
	assertEquals(1, parser.getMessages().size());
    assertEquals("Test id", parser.getMessages().get("102").getMessageId());
    assertEquals("Test str", parser.getMessages().get("102").getMessageString());
	assertEquals("102", parser.getMessages().get("102").getRowIdRow());
	assertEquals(true, parser.getMessages().get("102").getRowTrl());
  }
  
  @Test 
  public final void testRowTrlParsingWithTrueStrings() { 
	parser = new MessageParser();
	parser.parseLine(messageStartString);
	parser.parseLine(commentRowId);
	parser.parseLine("# RowTrl: N");
	parser.parseLine(testMsgId);
	parser.parseLine(testMsgStr);
	parser.parseLine("\n");
	assertEquals(1, parser.getMessages().size());
    assertEquals("Test id", parser.getMessages().get("102").getMessageId());
    assertEquals("Test str", parser.getMessages().get("102").getMessageString());
	assertEquals("102", parser.getMessages().get("102").getRowIdRow());
	assertEquals(true, parser.getMessages().get("102").getRowTrl());
  }

  @Test 
  public final void testRowTrlParsingWithMatchedStrings() { 
	parser = new MessageParser();
	parser.parseLine(messageStartString);
	parser.parseLine(commentRowId);
	parser.parseLine("# RowTrl: N");
	parser.parseLine(testMsgId);
	parser.parseLine("msgstr \"Test id\"\n");
	parser.parseLine("\n");
	assertEquals(1, parser.getMessages().size());
    assertEquals("Test id", parser.getMessages().get("102").getMessageId());
    assertEquals("Test id", parser.getMessages().get("102").getMessageString());
	assertEquals("102", parser.getMessages().get("102").getRowIdRow());
	assertEquals(false, parser.getMessages().get("102").getRowTrl());
  }

  @Test 
  public final void testRowTrlParsingWithMatchedStringsRowTrlTrue() { 
	parser = new MessageParser();
	parser.parseLine(messageStartString);
	parser.parseLine(commentRowId);
	parser.parseLine("# RowTrl: Y");
	parser.parseLine(testMsgId);
	parser.parseLine("msgstr \"Test id\"\n");
	parser.parseLine("\n");
	assertEquals(1, parser.getMessages().size());
    assertEquals("Test id", parser.getMessages().get("102").getMessageId());
    assertEquals("Test id", parser.getMessages().get("102").getMessageString());
	assertEquals("102", parser.getMessages().get("102").getRowIdRow());
	assertEquals(true, parser.getMessages().get("102").getRowTrl());
  }

  @Test 
  public final void testRowTrlParsingWithEmptyStringsRowTrlFalse() { 
	parser = new MessageParser();
	parser.parseLine(messageStartString);
	parser.parseLine(commentRowId);
	parser.parseLine("# RowTrl: N");
	parser.parseLine(testMsgId);
	parser.parseLine("msgstr \"\"\n");
	parser.parseLine("\n");
	assertEquals(1, parser.getMessages().size());
    assertEquals("Test id", parser.getMessages().get("102").getMessageId());
    assertEquals("", parser.getMessages().get("102").getMessageString());
	assertEquals("102", parser.getMessages().get("102").getRowIdRow());
	assertEquals(false, parser.getMessages().get("102").getRowTrl());
  }

  @Test
  public final void testMultipleMessageLines() {
    String[] strings = createMultipleMessages("102");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(1, parser.getMessages().size());
    strings = createMultipleMessages("103");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(2, parser.getMessages().size());
  }
  
  @Test
  public final void testMultipleMessageLinesWithDuplicateRowId() {
    String[] strings = createMultipleMessages("102");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(1, parser.getMessages().size());
    strings = createMultipleMessages("103");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(2, parser.getMessages().size());
    strings = createMultipleMessages("102");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(2, parser.getMessages().size());
    strings = createMultipleMessages("104");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(3, parser.getMessages().size());
  }
  
  @Test
  public final void testMultipleMessageLinesWithEmptyString() {
    String[] strings = createMultipleMessages("102");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(1, parser.getMessages().size());
    strings = createMultipleMessages("103");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(2, parser.getMessages().size());
    strings = createMultipleMessages("102");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(2, parser.getMessages().size());
    strings = createMultipleMessagesEmptyString("104");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(3, parser.getMessages().size());
  }
  
  @Test
  public void testMultipleStringWithCollisions() {
    String[] strings = createMultipleMessages("999");
    log4j.debug("strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(1, parser.getMessages().size());
    strings = createMultipleMessages("999");
    log4j.debug("testMultipleStringWithCollisions: strings.size(): " + strings.length);
    for (int i = 0; i < strings.length; i++) {
      parser.parseLine(strings[i]);
    }
    assertEquals(1, parser.getMessages().size());
    assertEquals("Java Version", parser.getMessages().get("999").getMessageId());
  }
  
  
  private String[] createMultipleMessages(String rowId) {
    String[] strings = new String[] {
        "\n",
        "# RowIds:\n",
        "# " + rowId + "\n",
        "# RowTrl: Y\n",
        "# Name: Java Version [" + rowId + "]\n",
        "# Description: Displays the version of the default Java VM [" + rowId + "]\n",
        "# Help: The java version used by the application might be different. [" + rowId + "]\n",
        "msgid \"Java Version\"\n",
        "msgstr \"Versione Java\"\n",
        "\n"};
    return strings;
  }
  
  private String[] createMultipleMessagesEmptyString(String rowId) {
    String[] strings = new String[] {
        "\n",
        "# RowIds:\n",
        "# " + rowId + "\n",
        "# RowTrl: Y\n",
        "# Name: Java Version [" + rowId + "]\n",
        "# Description: Displays the version of the default Java VM [" + rowId + "]\n",
        "# Help: The java version used by the application might be different. [" + rowId + "]\n",
        "msgid \"Java Version\"\n",
        "msgstr \"\"\n",
        "\n"};
    return strings;
  }
}