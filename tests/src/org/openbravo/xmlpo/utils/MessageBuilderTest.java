/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;
import org.openbravo.xmlpo.objects.ValueType;
import org.openbravo.xmlpo.utils.MessageBuilder;


public class MessageBuilderTest {
  
  private static final Logger log4j = Logger.getLogger(MessageBuilderTest.class);
  
  private RowObject rowObj;
  private ValueObject valueObj;
  private ValueType valueType;
  private RowObject rowObjEmptyMsgStr;
  private ValueType valueTypeEmptyMsgStr;
  private Message sampleMessage;
  private Message sampleMessageEmptyMsgStr;
  private String sampleOutput;
  
  @Before
  public void setUp() throws Exception {
    rowObj = new RowObject();
    rowObj.setRowId("5973945635C6FAB2E040A8C0846648BC");
    rowObj.setTranslation("Y");
    
    ValueObject valueObject = createValueObject("name", "name original", "name value");
    valueType = new ValueType(1, "name");
    valueObject.setType(valueType);
    rowObj.addValueObject(valueObject);
    
    valueObject = createValueObject("description", "description original", "description value");
    valueType = new ValueType(2, "description");
    valueObject.setType(valueType);
    rowObj.addValueObject(valueObject);
    
    valueObject = createValueObject("help", "help original", "help value");
    valueType = new ValueType(3, "help");
    valueObject.setType(valueType);
    rowObj.addValueObject(valueObject);
  }
  
  @Before
  public void setUpRowEmptyMsgStr() throws Exception {
    rowObjEmptyMsgStr = new RowObject();
    rowObjEmptyMsgStr.setRowId("5973945635C6FAB2E040A8C0846648BC");
    rowObjEmptyMsgStr.setTranslation("Y");
    
    ValueObject valueObject = createValueObject("name", "name original", "name original");
    valueTypeEmptyMsgStr = new ValueType(1, "name");
    valueObject.setType(valueTypeEmptyMsgStr);
    rowObjEmptyMsgStr.addValueObject(valueObject);
    
    valueObject = createValueObject("description", "description original", "description value");
    valueTypeEmptyMsgStr = new ValueType(2, "description");
    valueObject.setType(valueTypeEmptyMsgStr);
    rowObjEmptyMsgStr.addValueObject(valueObject);
    
    valueObject = createValueObject("help", "help original", "help value");
    valueTypeEmptyMsgStr = new ValueType(3, "help");
    valueObject.setType(valueTypeEmptyMsgStr);
    rowObjEmptyMsgStr.addValueObject(valueObject);
  }
  
  @Before
  public void setUpMessage() {
    sampleMessage = new Message();
    sampleMessage.setRowIdRow("# RowIds:\n# 5973945635C6FAB2E040A8C0846648BC\n");
    sampleMessage.setMessageId("msgid \"name original\"\n");
    sampleMessage.setMessageString("msgstr \"name value\"\n\n");
    sampleMessage.setComments("# RowTrl: Y\n# Name: name original [5973945635C6FAB2E040A8C0846648BC]\n# Description: description original [5973945635C6FAB2E040A8C0846648BC]\n# Help: help original [5973945635C6FAB2E040A8C0846648BC]\n");
    
    StringBuffer output = new StringBuffer();
    output.append(sampleMessage.getRowIdRow());
    output.append(sampleMessage.getCommentString());
    output.append(sampleMessage.getMessageId());
    output.append(sampleMessage.getMessageString());
    sampleOutput = output.toString();
  }
  
  @Before
  public void setUpMessageMatchingOriginalAndValue() {
    sampleMessageEmptyMsgStr = new Message();
    sampleMessageEmptyMsgStr.setRowIdRow("# RowIds:\n# 5973945635C6FAB2E040A8C0846648BC\n");
    sampleMessageEmptyMsgStr.setMessageId("msgid \"name original\"\n");
    sampleMessageEmptyMsgStr.setMessageString("msgstr \"name original\"\n\n");
    sampleMessageEmptyMsgStr.setComments("# RowTrl: Y\n# Name: name original [5973945635C6FAB2E040A8C0846648BC]\n# Description: description original [5973945635C6FAB2E040A8C0846648BC]\n# Help: help original [5973945635C6FAB2E040A8C0846648BC]\n");
    
    StringBuffer output = new StringBuffer();
    output.append(sampleMessageEmptyMsgStr.getRowIdRow());
    output.append(sampleMessageEmptyMsgStr.getCommentString());
    output.append(sampleMessageEmptyMsgStr.getMessageId());
    output.append(sampleMessageEmptyMsgStr.getMessageString());
  }
  
  @After
  public void tearDown() throws Exception {
    rowObj = null;
    valueObj = null;
    valueType = null;
    sampleOutput = "";
  }
  
  @After
  public void tearDownMessage() throws Exception {
    sampleMessage = null;
  }

  @Test
  public void testLongValueMessages() {
    RowObject testRrowObj = new RowObject();
    testRrowObj.setRowId("217");
    testRrowObj.setTranslation("Y");
    log4j.debug("TRANSLATION RESULT: " + testRrowObj.getTranslation());
    log4j.debug("Running the testLongValueMessages");
    
    String orig = "ATTENTION: ANY CHANGES here will effect the structure of your chart of accounts!\nFor changes to become effective, you must re-login and re-start the Application Server.";
    String value = "ATTENTION: ANY CHANGES here will effect the structure of your chart of accounts!\nFor changes to become effective, you must re-login and re-start the Application Server.";
    String msgIdResult = "msgid \"ATTENTION: ANY CHANGES here will effect the structure of your chart of accounts!\"\n\"For changes to become effective, you must re-login and re-start the Application Server.\"\n";
    String msgStrResult = "msgstr \"ATTENTION: ANY CHANGES here will effect the structure of your chart of accounts!\"\n\"For changes to become effective, you must re-login and re-start the Application Server.\"\n\n";
    String msgCommResult = "# RowTrl: Y\n# CommitWarning: ATTENTION: ANY CHANGES here will effect the structure of your chart of accounts!\n# For changes to become effective, you must re-login and re-start the Application Server. [217]\n";
    
    ValueObject valueObject = createValueObject("name", orig, value);
    ValueType testValueType = new ValueType(1, "CommitWarning");
    valueObject.setType(testValueType);
    testRrowObj.addValueObject(valueObject);
    
    MessageBuilder builder = new MessageBuilder(testRrowObj, testValueType);
    Message result = builder.buildMessage();
    assertEquals("MessageId not as expected.", msgIdResult, result.getMessageId());
    assertEquals("MessageString not as expected.", msgStrResult, result.getMessageString());
    assertEquals("Message comment string not as expected.", msgCommResult, result.getCommentString());
    
    log4j.debug("value of result comment string: " + result.getCommentString());
  }

  @Test
  public void testEscapeQuotedMessages() {
    RowObject testRrowObj = new RowObject();
    testRrowObj.setRowId("217");
    testRrowObj.setTranslation("Y");
    log4j.debug("TRANSLATION RESULT: " + testRrowObj.getTranslation());
    log4j.debug("Running the testEscapeQutoedMessages");
    
    String orig = "ATTENTION: ANY CHANGES \\\"here\\\" will effect the structure of your chart of accounts!";
    String value = "ATTENTION: ANY CHANGES \\\"here\\\" will effect the &quot;structure&quot; of your chart of accounts!";
    String msgIdResult = "msgid \"ATTENTION: ANY CHANGES \\\\\\\"here\\\\\\\" will effect the structure of your chart of accounts!\"\n";
    String msgStrResult = "msgstr \"ATTENTION: ANY CHANGES \\\\\\\"here\\\\\\\" will effect the &quot;structure&quot; of your chart of accounts!\"\n\n";
    String msgCommResult = "# RowTrl: Y\n# CommitWarning: ATTENTION: ANY CHANGES \\\"here\\\" will effect the structure of your chart of accounts! [217]\n";
    
    ValueObject valueObject = createValueObject("name", orig, value);
    ValueType testValueType = new ValueType(1, "CommitWarning");
    valueObject.setType(testValueType);
    testRrowObj.addValueObject(valueObject);
    
    MessageBuilder builder = new MessageBuilder(testRrowObj, testValueType);
    Message result = builder.buildMessage();
    assertEquals("MessageId not as expected.", msgIdResult, result.getMessageId());
    assertEquals("MessageString not as expected.", msgStrResult, result.getMessageString());
    assertEquals("Message comment string not as expected.", msgCommResult, result.getCommentString());
    
    log4j.debug("value of result comment string: " + result.getCommentString());
  }
  
  @Test
  public void testMessageBuilder() {
    valueType = new ValueType(1, "name");
    MessageBuilder builder = new MessageBuilder(rowObj, valueType);
    
    assertEquals(3, builder.getComments().length);
    assertEquals(1, builder.getValueType().getIntValue());
  }
  
  @Test
  public void testBuildMessage() {
    MessageBuilder builder = new MessageBuilder(rowObj, new ValueType(1, "name"));
    
    assertEquals(3, builder.getComments().length);
    assertEquals(1, builder.getValueType().getIntValue());
    
    Message result = builder.buildMessage();
    assertEquals("MessageId not as expected.", sampleMessage.getMessageId(), result.getMessageId());
    assertEquals("MessageString not as expected.", sampleMessage.getMessageString(), result.getMessageString());
    log4j.debug("value of result comment string: " + result.getCommentString());
    assertEquals("Comment String not as expected.", sampleMessage.getCommentString(), result.getCommentString());
    assertEquals("MessageRowId not as expected.", sampleMessage.getRowIdRow(), result.getRowIdRow());
  }
  
  @Test
  public void testBuildMessageIgnoreTrlTrueRowTrlTrue() {
    rowObjEmptyMsgStr.setTranslation("Y");
    MessageBuilder builder = new MessageBuilder(rowObjEmptyMsgStr, new ValueType(1, "name"));
    builder.setIgnoreNonTrl(true);
    assertEquals(3, builder.getComments().length);
    assertEquals(1, builder.getValueType().getIntValue());
    
    String commentResult = "# RowTrl: Y\n# Name: name original [5973945635C6FAB2E040A8C0846648BC]\n# Description: description original [5973945635C6FAB2E040A8C0846648BC]\n# Help: help original [5973945635C6FAB2E040A8C0846648BC]\n";
    
    Message result = builder.buildMessage();
    assertEquals("MessageId not as expected.", "msgid \"name original\"\n", result.getMessageId());
    assertEquals("MessageString not as expected.", "msgstr \"name original\"\n\n", result.getMessageString());
    log4j.debug("value of result comment string: " + result.getCommentString());
    log4j.debug("value of result MsgStr: " + result.getMessageString());
    assertEquals("Comment String not as expected.", commentResult, result.getCommentString());
    assertEquals("MessageRowId not as expected.", sampleMessageEmptyMsgStr.getRowIdRow(), result.getRowIdRow());
  }
  
  @Test
  public void testBuildMessageIgnoreTrlTrueRowTrlFalse() {
    rowObjEmptyMsgStr.setTranslation("N");
    MessageBuilder builder = new MessageBuilder(rowObjEmptyMsgStr, new ValueType(1, "name"));
    builder.setIgnoreNonTrl(true);
    assertEquals(3, builder.getComments().length);
    assertEquals(1, builder.getValueType().getIntValue());
    
    String commentResult = "# RowTrl: N\n# Name: name original [5973945635C6FAB2E040A8C0846648BC]\n# Description: description original [5973945635C6FAB2E040A8C0846648BC]\n# Help: help original [5973945635C6FAB2E040A8C0846648BC]\n";
    
    Message result = builder.buildMessage();
    assertEquals("MessageId not as expected.", "msgid \"name original\"\n", result.getMessageId());
    assertEquals("MessageString not as expected.", "msgstr \"\"\n\n", result.getMessageString());
    log4j.debug("value of result comment string: " + result.getCommentString());
    log4j.debug("value of result MsgStr: " + result.getMessageString());
    assertEquals("Comment String not as expected.", commentResult, result.getCommentString());
    assertEquals("MessageRowId not as expected.", sampleMessageEmptyMsgStr.getRowIdRow(), result.getRowIdRow());
  }
  
  @Test
  public void testBuildMessageIgnoreTrlFalseRowTrlTrue() {
    rowObjEmptyMsgStr.setTranslation("Y");
    MessageBuilder builder = new MessageBuilder(rowObjEmptyMsgStr, new ValueType(1, "name"));
    builder.setIgnoreNonTrl(false);
    assertEquals(3, builder.getComments().length);
    assertEquals(1, builder.getValueType().getIntValue());
    
    String commentResult = "# RowTrl: Y\n# Name: name original [5973945635C6FAB2E040A8C0846648BC]\n# Description: description original [5973945635C6FAB2E040A8C0846648BC]\n# Help: help original [5973945635C6FAB2E040A8C0846648BC]\n";
    
    Message result = builder.buildMessage();
    assertEquals("MessageId not as expected.", "msgid \"name original\"\n", result.getMessageId());
    assertEquals("MessageString not as expected.", "msgstr \"name original\"\n\n", result.getMessageString());
    log4j.debug("value of result comment string: " + result.getCommentString());
    log4j.debug("value of result MsgStr: " + result.getMessageString());
    assertEquals("Comment String not as expected.", commentResult, result.getCommentString());
    assertEquals("MessageRowId not as expected.", sampleMessageEmptyMsgStr.getRowIdRow(), result.getRowIdRow());
  }
  
  @Test
  public void testBuildMessageIgnoreTrlFalseRowTrPOFileBuilderlFalse() {
    rowObjEmptyMsgStr.setTranslation("N");
    MessageBuilder builder = new MessageBuilder(rowObjEmptyMsgStr, new ValueType(1, "name"));
    builder.setIgnoreNonTrl(false);
    assertEquals(3, builder.getComments().length);
    assertEquals(1, builder.getValueType().getIntValue());
    
    String commentResult = "# RowTrl: N\n# Name: name original [5973945635C6FAB2E040A8C0846648BC]\n# Description: description original [5973945635C6FAB2E040A8C0846648BC]\n# Help: help original [5973945635C6FAB2E040A8C0846648BC]\n";
    
    Message result = builder.buildMessage();
    assertEquals("MessageId not as expected.", "msgid \"name original\"\n", result.getMessageId());
    assertEquals("MessageString not as expected.", "msgstr \"name original\"\n\n", result.getMessageString());
    log4j.debug("value of result comment string: " + result.getCommentString());
    log4j.debug("value of result MsgStr: " + result.getMessageString());
    assertEquals("Comment String not as expected.", commentResult, result.getCommentString());
    assertEquals("MessageRowId not as expected.", sampleMessageEmptyMsgStr.getRowIdRow(), result.getRowIdRow());
  }
  
  @Test
  public void testCreateMessageString() {
    MessageBuilder builder = new MessageBuilder(rowObj, new ValueType(1, "name"));
    
    assertEquals(3, builder.getComments().length);
    assertEquals(1, builder.getValueType().getIntValue());
    
    Message result = builder.buildMessage();
    
    assertEquals("MessageString not as expected.", sampleOutput, builder.getMessageString(result));
    log4j.debug("Output String: " + builder.getMessageString(result));
  }
  
  @Test
  public void testCreateMessageNewLineHandling() {
    RowObject rObj = new RowObject();
    rObj.setRowId("102");
    rObj.setTranslation("Y");
    
    ValueObject vObj = createValueObject("name", "\\n", "\\n");
    vObj.setType(new ValueType(1, "name"));
    rObj.addValueObject(vObj);
    
    MessageBuilder builder = new MessageBuilder(rObj, new ValueType(1, "name"));
    builder.setIgnoreNonTrl(false);
    
    Message result = builder.buildMessage();
    
    assertEquals(1, builder.getComments().length);
    assertEquals(1, builder.getValueType().getIntValue());
    
    assertEquals("MessageId not as expected.", "msgid \"\\n\"\n", result.getMessageId());
    assertEquals("MessageString not as expected.", "msgstr \"\\n\"\n\n", result.getMessageString());
  }
  
  private ValueObject createValueObject(String column, String original, String value) {
    valueObj = new ValueObject();
    valueObj.setColumn(column);
    valueObj.setOriginal(original);
    valueObj.setValue(value);
    return valueObj;
  }
}
