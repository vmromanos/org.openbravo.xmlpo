/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;

public class RowObjectTest {

  private RowObject rowObject;
  private RowObject defaultObject;
  
  @Before
  public void setUp() throws Exception {
    rowObject = new RowObject();
    
    defaultObject = new RowObject();
    defaultObject.setRowId("55475CE61F5C3E65E040007F01016323");
    defaultObject.setTranslation("Y");
    
    defaultObject.addValueObject(createValueObject("object1Col", "object1Orig", "object1Val"));
    defaultObject.addValueObject(createValueObject("object2Col", "object2Orig", "object2Val"));
    defaultObject.addValueObject(createValueObject("object3Col", "object3Orig", "object3Val"));
    
  }
  
  @After
  public void tearDown() throws Exception {
    rowObject = null;
    defaultObject = null;
  }
  
  @Test
  public void testGetRowId() {
    assertEquals("55475CE61F5C3E65E040007F01016323", defaultObject.getRowId());
  }
  
  @Test
  public void testSetRowId() {
    rowObject.setRowId("100022");
    assertEquals("100022", rowObject.getRowId());
  }
  
  @Test
  public void testGetTranslation() {
    assertEquals(true, defaultObject.getTranslation());
  }
  
  @Test
  public void testSetTranslation() {
    rowObject.setTranslation("y");
    assertEquals(true, rowObject.getTranslation());
    rowObject.setTranslation("N");
    assertEquals(false, rowObject.getTranslation());
    rowObject.setTranslation("n");
    assertEquals(false, rowObject.getTranslation());
    rowObject.setTranslation("test");
    assertEquals(false, rowObject.getTranslation());

  }
  
  @Test
  public void testGetValueObjects() {
    assertEquals(3, defaultObject.getValueObjects().size());
  }
  
  @Test
  public void testSetValueObjects() {
    //fail("Not yet implemented"); 
    // TODO
  }
  
  @Test
  public void testAddValueObject() {
    rowObject.addValueObject(createValueObject("testCol", "testOrig", "testValue"));
    assertEquals(1, rowObject.getValueObjects().size());
  }
  
  @Test
  public void testCompare() {
    RowObject row1 = new RowObject();
    row1.setRowId("1");
    RowObject row2 = new RowObject();
    row2.setRowId("2");
    int result = rowObject.compare(row1, row2);
    assertEquals(-1, result);
    row1.setRowId("101");
    row2.setRowId("10001");
    result = rowObject.compare(row1, row2);
    assertEquals(1, result);
    row1.setRowId("10100");
    row2.setRowId("10001");
    result = rowObject.compare(row1, row2);
    assertEquals(1, result);
  }
  
  private ValueObject createValueObject(String column, String original, String value) {
    ValueObject object = new ValueObject();
    object.setColumn(column);
    object.setOriginal(original);
    object.setValue(value);
    return object;
  }
}
