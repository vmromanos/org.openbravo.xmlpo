package org.openbravo.xmlpo.objects;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class POFileObjectTest {
  
  private POFileObject object;
  
  @Before
  public void setUp() throws Exception {
    object = new POFileObject();
  }
  
  @After
  public void tearDown() throws Exception {
    object = null;
  }
  
  @Test
  public void testPOFileObject() {
    assertNotNull(object.getMessages());
  }
  
  @Test
  public void testGetFileName() {
    String testFileName = "testFileName";
    object.setFileName(testFileName);
    
    POFileObject newObj = object;
    assertEquals("Filename not as expected.", testFileName, newObj.getFileName());
  }
  
  @Test
  public void testGetTableName() {
    String testTableName = "testTableName";
    object.setTableName(testTableName);
    POFileObject newObj = object;
    assertEquals("Tablename not as expected.", testTableName, newObj.getTableName());
  }
  
  @Test
  public void testGetColumnName() {
    String testColumnName = "testColumnName";
    object.setColumnName(testColumnName);
    POFileObject newObj = object;
    assertEquals("Columnname not as expected.", testColumnName, newObj.getColumnName());
  }
  
  @Test
  public void testGetColumnIndex() {
    Integer testColumnIndex = new Integer(21);
    object.setColumnIndex(testColumnIndex.toString());
    POFileObject newObj = object;
    assertEquals("ColumnIndex not as expected.", testColumnIndex.intValue(), newObj.getColumnIndex());
  }
  
  @Test
  public void testGetLanguage() {
    String testLanguage = "testLanguage";
    object.setLanguage(testLanguage);
    POFileObject newObj = object;
    assertEquals("Language not as expected.", testLanguage, newObj.getLanguage());
  }
  
  @Test
  public void testGetVersion() {
    String testVersion = "testVersion";
    object.setVersion(testVersion);
    POFileObject newObj = object;
    assertEquals("Version not as expected.", testVersion, newObj.getVersion());
  }
  
  @Test
  public void testGetMessages() {
    //fail("Not yet implemented"); // TODO
  }
  
  @Test
  public void testSetMessages() {
    //fail("Not yet implemented"); // TODO
  }
  
  @Test
  public void testAddMessages() {
    //fail("Not yet implemented"); // TODO
  }
  
}
