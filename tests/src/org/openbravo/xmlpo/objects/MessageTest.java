/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.Message;

public class MessageTest {
  
  private Message message;
  private Message defaultMessage;
  
  @Before
  public void setUp() throws Exception {
    message = new Message();
    
    defaultMessage = new Message();
    defaultMessage.setRowIdRow("# RowIds:\n# 5973945635C6FAB2E040A8C0846648BC\n");
    defaultMessage.setComments("# Name: name original [5973945635C6FAB2E040A8C0846648BC] \n# Description: description original [5973945635C6FAB2E040A8C0846648BC]\n# Help: help original [5973945635C6FAB2E040A8C0846648BC]\n");
    defaultMessage.setMessageId("msgid \"This is the original\"\n");
    defaultMessage.setMessageString("msgstr \"This is the datavalue\"\n");
    
  }
  
  @After
  public void tearDown() throws Exception {
    message = null;
    defaultMessage = null;
  }
  
  @Test
  public void testGetRowIdRow() {
    assertEquals("# RowIds:\n# 5973945635C6FAB2E040A8C0846648BC\n", defaultMessage.getRowIdRow());
  }
  
  @Test
  public void testSetRowIdRow() {
    message.setRowIdRow("test setting rowId");
    assertEquals("test setting rowId", message.getRowIdRow());
  }
  
  @Test
  public void testGetMessageId() {
    assertEquals("msgid \"This is the original\"\n", defaultMessage.getMessageId());
  }
  
  @Test
  public void testSetMessageId() {
    message.setMessageId("test messasge Id");
    assertEquals("test messasge Id", message.getMessageId());
  }
  
  @Test
  public void testGetMessageString() {
    assertEquals("msgstr \"This is the datavalue\"\n", defaultMessage.getMessageString());
  }
  
  @Test
  public void testSetMessageString() {
    message.setMessageString("test messasge string");
    assertEquals("test messasge string", message.getMessageString());
  }
  
  @Test
  public void testGetCommentString() {
    assertEquals("# Name: name original [5973945635C6FAB2E040A8C0846648BC] \n# Description: description original [5973945635C6FAB2E040A8C0846648BC]\n# Help: help original [5973945635C6FAB2E040A8C0846648BC]\n", defaultMessage.getCommentString());
  }
  
  @Test
  public void testSetComments() {
    message.setComments("test comment string");
    assertEquals("test comment string", message.getCommentString());
  }
  
}
