/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.merge;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.merge.MessageMerge;
import org.openbravo.xmlpo.objects.Message;


public class TestMessageMerge {

  private MessageMerge merge;
  
  @Before
  public void setUp() throws Exception {
    merge = new MessageMerge();
  }
  
  @After
  public void tearDown() throws Exception {
    merge = null;
  }
  
  @Test
  public final void testMessageMerge() {
    assertNotNull(merge.getMessage());
  }
  
  @Test
  public final void testGetMessage() {
    assertNotNull(merge.getMessage());
  }
  
  @Test
  public final void testMergeMessages() {
    String commentOne = "# RowIds:\n# 5973945635C6FAB2E040A8C0846648BC\n# Name: name original\n";
    String commentTwo = "# RowIds:\n# 5973945635C6FAB2E040A8C0846648BC\n# Name: name original2\n";
    Message first = createMessage("Msg Id", "Msg String", commentOne, "5973945635C6FAB2E040A8C0846648BC");
    Message second = createMessage("Msg Id2", "Msg String2", commentTwo, "5973945635C6FAB2E040A8C0846648BC");
    merge.setFirstMessage(first);
    merge.setSecondMessage(second);
    merge.mergeMessages();
    assertEquals("<<<" + "Msg Id" + ">>>" + "<<<" + "Msg Id2" + ">>>", merge.getMessage().getMessageId());
    assertEquals("<<<" + "Msg String" + ">>>" + "<<<" + "Msg String2" + ">>>", merge.getMessage().getMessageString());
    assertEquals(commentOne + commentTwo + "# DUPLICATION\n", merge.getMessage().getCommentString());
  }
  
  @Test
  public final void testConflictingIdButIdentical() {
    String commentOne = "# RowIds:\n# 5973945635C6FAB2E040A8C0846648BC\n# Name: name original\n";
    String commentTwo = "# RowIds:\n# 5973945635C6FAB2E040A8C0846648BC\n# Name: name original2\n";
    Message first = createMessage("Msg Id", "Msg String", commentOne, "5973945635C6FAB2E040A8C0846648BC");
    Message second = createMessage("Msg Id", "Msg String", commentTwo, "5973945635C6FAB2E040A8C0846648BC");
    merge.setFirstMessage(first);
    merge.setSecondMessage(second);
    merge.mergeMessages();
    assertEquals("Msg Id", merge.getMessage().getMessageId());
    assertEquals("Msg String", merge.getMessage().getMessageString());
    assertEquals(commentOne, merge.getMessage().getCommentString());
  }
  
  @Test
  public final void testConflictingIdIdenticalMsgIdOnly() {
    String commentOne = "# RowIds:\n# 102\n# Name: name original\n";
    String commentTwo = "# RowIds:\n# 102\n# Name: name original2\n";
    Message first = createMessage("Msg Id", "Msg String", commentOne, "102");
    Message second = createMessage("Msg Id", "Msg String2", commentTwo, "102");
    merge.setFirstMessage(first);
    merge.setSecondMessage(second);
    merge.mergeMessages();
    assertEquals("Msg Id", merge.getMessage().getMessageId());
    assertEquals("<<<" + "Msg String" + ">>>" + "<<<" + "Msg String2" + ">>>", merge.getMessage().getMessageString());
    assertEquals(commentOne + commentTwo + "# DUPLICATION\n", merge.getMessage().getCommentString());
  }
  
  @Test
  public final void testConflictingIdIdenticalMsgStrOnly() {
    String commentOne = "# RowIds:\n# 102\n# Name: name original\n";
    String commentTwo = "# RowIds:\n# 102\n# Name: name original2\n";
    Message first = createMessage("Msg Id", "Msg String", commentOne, "102");
    Message second = createMessage("Msg Id2", "Msg String", commentTwo, "102");
    merge.setFirstMessage(first);
    merge.setSecondMessage(second);
    merge.mergeMessages();
    assertEquals("<<<" + "Msg Id" + ">>>" + "<<<" + "Msg Id2" + ">>>", merge.getMessage().getMessageId());
    assertEquals("Msg String", merge.getMessage().getMessageString());
    assertEquals(commentOne + commentTwo + "# DUPLICATION\n", merge.getMessage().getCommentString());
  }
  
  private Message createMessage(String msgId, String msgStr, String comment, String rowId) {
    Message newMsg = new Message();
    newMsg.setRowIdRow(rowId);
    newMsg.setMessageId(msgId);
    newMsg.setMessageString(msgStr);
    newMsg.setComments(comment);
    return newMsg;
  }
}
