/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/


package org.openbravo.xmlpo.xml2po;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileReader;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author phillip
 *
 */
public class TestXML2POMain {
  
  private static final Logger log4j = Logger.getLogger(TestXML2POMain.class);
  
  private String outputFolderString;
  private String inputFolder;
  private String fileString;
  private String fileName;
  
  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    fileString = "tests/resources/AD_TASK_TRL_en_US.xml";
    inputFolder = "tests/resources/input/";
    outputFolderString = "tests/resources/output/";
    fileName = "AD_TASK_TRL_en_US.xml";
  }
  
  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    fileString = "";
    inputFolder = "";
    outputFolderString = "";
    fileName = "";
  }
  
  @Test
  public void testMain() {
    try {
      log4j.info("### TESTMAIN ###");
      XML2POMain.main(new String[] { "", "", "false", fileString });
      FileReader inReader = new FileReader(new File("tests/resources/AD_TASK_Name_en_US.po"));
      assertEquals("UTF8", inReader.getEncoding());
    } catch (Exception e) {
      fail("valid argument passed." + e.getMessage());
    }
  }

  @Test
  public void testMainWithValidFolderStrings() {
    try {
      log4j.debug("### TESTMAINWITHVALIDFOLDERSTRINGS ###");
      XML2POMain.main(new String[] { outputFolderString, inputFolder, "false", fileName });
    } catch (Exception e) {
      fail("valid argument passed." + e.getMessage());
    }
  }
  
  @Test
  public void testMainWithValidFolderNoFiles() {
    try {
      log4j.debug("### TESTMAINWITHVALIDFOLDERNOFILES ###");
      XML2POMain.main(new String[] { outputFolderString, inputFolder, "false" });
    } catch (Exception e) {
      fail("valid argument passed." + e.getMessage());
    }
  }
  

  @Test
  public void testSettingFolder() {
    try {
      XML2POMain.main(new String[] { 
           "tests/resources/liveSample/output/"  //outputfolder 
           ,"tests/resources/liveSample/"         //inputFolder
           ,"false"
           ,"AD_TASK_TRL_it_IT_CBT.xml"           
           ,"AD_TASK_TRL_it_IT_Kelyon.xml"
           });
    } catch (Exception e) {
      assertNotNull(e.getMessage());
    }
  }
  
  @Test
  public void testInvalidFolderExceptionHandled() {
    try {
      log4j.debug("### TEST_INVALID_FOLDER_EXCEPTION_HANDLED ###");
      XML2POMain.main(new String[] { "invalidOutputFolderString", inputFolder, "false" });
      fail("InvalidFolderException should have been caught.");
    } catch (Exception e) {
      assertEquals("Exception not equal", "Output folder (invalidOutputFolderString/) does not exist.", e.getMessage());
    }
  }
  
  @Test
  public void testInvalidFolderExceptionHandledInputFolder() {
    try {
      log4j.debug("### TEST_INVALID_FOLDER_EXCEPTION_HANDLED_INPUT_FOLDER ###");
      XML2POMain.main(new String[] { outputFolderString, "invalid input folder", "false" });
      fail("InvalidFolderException should have been caught.");
    } catch (Exception e) {
      assertEquals("Exception not equal", "Input folder (invalid input folder/) does not exist.", e.getMessage());
    }
  }
  
  @Test
  public void testInvalidFolderExceptionHandledNotFolder() {
    try {
      log4j.debug("### TEST_INVALID_FOLDER_EXCEPTION_HANDLED_NOT_FOLDER ###");
      XML2POMain.main(new String[] { outputFolderString, inputFolder + fileName, "false" });
      fail("InvalidFolderException should have been caught.");
    } catch (Exception e) {
      assertEquals("Exception not equal", "Input folder (" + inputFolder + fileName + "/) is not a folder.", e.getMessage());
    }
  }
}
