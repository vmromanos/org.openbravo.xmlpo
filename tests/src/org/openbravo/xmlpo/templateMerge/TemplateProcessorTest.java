/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.templateMerge;

import static org.junit.Assert.*;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.TaskProcess;
import org.openbravo.xmlpo.objects.ValueObject;

public class TemplateProcessorTest {
	
	private TaskProcess taskProc;
	private TemplateProcessor proc;
	
	@Before
	public void setUp() throws Exception{
		createTaskProcess();
		proc = new TemplateProcessor(taskProc);
		createFileArrays();
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testTemplateProcessor() {
		TemplateProcessor proc = new TemplateProcessor(taskProc);
		assertNotNull(proc.getTaskProcess());
	}

	@Test
	public void testProcessTemplate() {
		TemplateProcessor proc = new TemplateProcessor(taskProc);
		proc.processFiles();
		assertNotNull(proc.getTemplateMap());
		assertNotNull(proc.getBaseMap());
		assertTrue(proc.getTemplateMap().containsKey("AD_TASK"));
		assertTrue(proc.getBaseMap().containsKey("AD_TASK"));
		assertNotNull(proc.getTemplateMap().get("AD_TASK"));
	}
	
	@Test
	public void testProcessTemplateFileObj() {
		//TemplateProcessor proc = new TemplateProcessor(taskProc);
		proc.processFiles();
		FileObject fileObj = proc.getTemplateMap().get("AD_TASK");
		assertNotNull(fileObj);
		assertEquals("es_ES", fileObj.getLanguage());
		assertEquals("2.40", fileObj.getVersion());
		assertEquals(5, fileObj.getRowObjects().size());
		boolean valueFound = false;
		for (Iterator<RowObject> iterator = fileObj.getRowObjects().iterator(); iterator.hasNext();) {
			RowObject row = (RowObject) iterator.next();
			if (row.getRowId().equalsIgnoreCase("105")) {
				assertEquals(1, row.getValueObjects().size());
				for (Iterator<ValueObject> iterator2 = row.getValueObjects().iterator(); iterator2
						.hasNext();) {
					ValueObject type = (ValueObject) iterator2.next();
					assertEquals("Name", type.getColumn());
					assertEquals("Black", type.getOriginal());
				}
				valueFound = true;
			} else if (row.getRowId().equals("106")) {
				fail();
			}
		}
		assertTrue(valueFound);
	}

	@Test
	public void testProcessBaseFileObj() {
		//TemplateProcessor proc = new TemplateProcessor(taskProc);
		proc.processFiles();
		FileObject fileObj = proc.getBaseMap().get("AD_TASK");
		assertNotNull(fileObj);
		assertEquals("es_ES", fileObj.getLanguage());
		assertEquals("2.40", fileObj.getVersion());
		assertEquals(5, fileObj.getRowObjects().size());
		boolean valueFound = false;
		for (Iterator<RowObject> iterator = fileObj.getRowObjects().iterator(); iterator.hasNext();) {
			RowObject row = (RowObject) iterator.next();
			if (row.getRowId().equalsIgnoreCase("105")) {
				assertEquals(1, row.getValueObjects().size());
				for (Iterator<ValueObject> iterator2 = row.getValueObjects().iterator(); iterator2
						.hasNext();) {
					ValueObject type = (ValueObject) iterator2.next();
					assertEquals("Name", type.getColumn());
					assertEquals("black", type.getOriginal());
					assertEquals("Negre", type.getValue());
				}
				valueFound = true;
			} else if (row.getRowId().equals("103")) {
				fail();
			}
		}
		assertTrue(valueFound);
	}

	@Test
	public void testGetResult() {
		//TemplateProcessor process = new TemplateProcessor(taskProc);
		proc.processFiles();
		assertNotNull("mergeFileObjects is null.", proc.getMergedFileObjects());
		assertEquals(1, proc.getMergedFileObjects().size());
		for (Iterator<FileObject> iterator = proc.getMergedFileObjects().iterator(); iterator.hasNext();) {
			FileObject mergeObj = (FileObject) iterator.next();
			assertEquals("AD_TASK", mergeObj.getTableName());
			assertEquals("es_ES",mergeObj.getLanguage());
			assertEquals(5, mergeObj.getRowObjects().size());
			HashMap<String, RowObject> rowMap = new HashMap<String, RowObject>();
			for (Iterator<RowObject> iterator2 = mergeObj.getRowObjects().iterator(); iterator2
					.hasNext();) {
				RowObject row = (RowObject) iterator2.next();
				rowMap.put(row.getRowId(), row);
			}
			// row 102 <row id="102" trl="Y">
			// row value: <value column="Name" original="Blue">Azul</value>
			if (rowMap.containsKey("102")) {
				RowObject obj = rowMap.get("102");
				assertTrue("Row 102 not set to translated: ", obj.getTranslation());
				assertEquals(1, obj.getValueObjects().size());
				for (Iterator<ValueObject> iterator2 = obj.getValueObjects().iterator(); iterator2
						.hasNext();) {
					ValueObject value = (ValueObject) iterator2.next();
					assertEquals("Name", value.getColumn());
					assertEquals("Blue", value.getOriginal());
					assertEquals("Azul", value.getValue());
				}
			} else {
				fail();
			}
			/*
			 * <row id="103" trl="N">
			 * <value column="Name" original="Red"></value>
			 * </row>
			 */
			if (rowMap.containsKey("103")) {
				RowObject obj = rowMap.get("103");
				assertFalse("Row 103 should not be set to translated: ", obj.getTranslation());
				assertEquals(1, obj.getValueObjects().size());
				for (Iterator<ValueObject> iterator2 = obj.getValueObjects().iterator(); iterator2
						.hasNext();) {
					ValueObject value = (ValueObject) iterator2.next();
					assertEquals("Name", value.getColumn());
					assertEquals("Red", value.getOriginal());
					assertEquals("", value.getValue());
				}
			} else {
				fail();
			}
			
			/*
			 * <row id="104" trl="N">
			 * <value column="Name" original="Light Green"></value>
			 * </row>
			 */
			if (rowMap.containsKey("104")) {
				RowObject obj = rowMap.get("104");
				assertFalse("Row 104 should not be set to translated: ", obj.getTranslation());
				assertEquals(1, obj.getValueObjects().size());
				for (Iterator<ValueObject> iterator2 = obj.getValueObjects().iterator(); iterator2
						.hasNext();) {
					ValueObject value = (ValueObject) iterator2.next();
					assertEquals("Name", value.getColumn());
					assertEquals("Light Green", value.getOriginal());
					assertEquals("Verde", value.getValue());
				}
			} else {
				fail();
			}
			
			/*
			 * <row id="105" trl="Y">
			 * <value column="Name" original="Black">Negre</value>
			 * </row>
			 */
			if (rowMap.containsKey("105")) {
				RowObject obj = rowMap.get("105");
				assertTrue("Row 105 not set to translated: ", obj.getTranslation());
				assertEquals(1, obj.getValueObjects().size());
				for (Iterator<ValueObject> iterator2 = obj.getValueObjects().iterator(); iterator2
						.hasNext();) {
					ValueObject value = (ValueObject) iterator2.next();
					assertEquals("Name", value.getColumn());
					assertEquals("Black", value.getOriginal());
					assertEquals("Negre", value.getValue());
				}
			} else {
				fail();
			}
			
			// rowId 106 should not exist
			if (rowMap.containsKey("106")) {
				fail("Row 106 should not exist in the result set but does.");
			}
			
			/*
			 * <row id="107" trl="N">
			 * <value column="Name" original="Orange"></value>
			 * </row>
			 */
			if (rowMap.containsKey("107")) {
				RowObject obj = rowMap.get("107");
				assertFalse("Row 107 should not be set to translated: ", obj.getTranslation());
				assertEquals(1, obj.getValueObjects().size());
				for (Iterator<ValueObject> iterator2 = obj.getValueObjects().iterator(); iterator2
						.hasNext();) {
					ValueObject value = (ValueObject) iterator2.next();
					assertEquals("Name", value.getColumn());
					assertEquals("Orange", value.getOriginal());
					assertEquals("", value.getValue());
				}
			} else {
				fail();
			}
		}
	}
	
	@Test
	public void generateResultFile() {
		proc.processFiles();
		proc.buildXMLOutput();
	}

	private void createTaskProcess() {
		taskProc = new TaskProcess();
		taskProc.setOutputFolder("tests/resources/templateMerge/output/testGenerated/");
		taskProc.setInputTemplateFolder("tests/resources/templateMerge/template/");
		taskProc.setInputBaseFolder("tests/resources/templateMerge/base/");
	}
	
	private void createFileArrays() {
		File[] templates = new File[] {new File("tests/resources/templateMerge/template/AD_TASK_TRL_Template.xml")};
		File[] baseFiles = new File[] {new File("tests/resources/templateMerge/base/AD_TASK_TRL_es_ES.xml")};
		taskProc.setTemplateFiles(templates);
		taskProc.setBaseFiles(baseFiles);
	}
}
