/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.templateMerge;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.exceptions.InvalidFolderException;

public class TestTemplateMerge {
	
	private TemplateMerge merge;
	private String inputTemplateFolder;
	private String inputBaseFolder;
	private String outputFolder;

	@Before
	public void setUp() throws Exception {
		merge = new TemplateMerge();
		inputTemplateFolder = "tests/resources/templateMerge/template";
		inputBaseFolder = "tests/resources/templateMerge/base";
		outputFolder = "tests/resources/templateMerge/output";
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void templateMerge() {
		System.out.println("TEST templateMerge");
		try {
			merge.main(new String[] {outputFolder, inputTemplateFolder, inputBaseFolder});
		} catch (Exception e) {
			System.out.println(e.getMessage());
			fail("Failed to run TemplateMerge" + e.getMessage());
		}
		assertEquals("OutputFolder not as expected. ", outputFolder + "/", merge.getTaskProcess().getOutputFolder());
		assertEquals("InputTemplateFolder not as expected. ", inputTemplateFolder + "/", merge.getTaskProcess().getInputTemplateFolder());
		assertEquals("InputBaseFolder not as expected. ", inputBaseFolder + "/", merge.getTaskProcess().getInputBaseFolder());
	}
	
	@Test
	public void templateMergeNoArgs() {
		System.out.println("TEST templateMergeNoArgs");
		try {
			merge.main(new String[] {});
		} catch (Exception e) {
			fail("Failed to run TemplateMergeNoArgs: " + e.getMessage());
		}
		assertEquals("OutputFolder not as expected. ", "", merge.getTaskProcess().getOutputFolder());
		assertEquals("InputTemplateFolder not as expected. ", "", merge.getTaskProcess().getInputTemplateFolder());
		assertEquals("InputBasFolder not as expected. ", "", merge.getTaskProcess().getInputBaseFolder());
	}
	
	@Test
	public void templateMergeInvalidTemplateFolder() {
		String invalidTempArg = "tests/resources/templateMerge/template/AD_Task_TRL_Template.xml";
		try {
			merge.main(new String[] {outputFolder, invalidTempArg, inputBaseFolder});
			fail();
		} catch (Exception e) {
			assertEquals(e.getClass(), InvalidFolderException.class);
		}
	}
	
	@Test
	public void templateMergeInvalidBaseFolder() {
		String invalidBaseArg = "tests/resources/templateMerge/base/AD_Task_TRL_es_ES.xml";
		try {
			merge.main(new String[] {outputFolder, inputTemplateFolder, invalidBaseArg});
			fail();
		} catch (Exception e) {
			assertEquals(e.getClass(), InvalidFolderException.class);
		}
	}
	
	@Test
	public void templateMergeInvalidOutputFolder() {
		String invalidOutputArg = "tests/resources/templateMerge/output/AD_Task_TRL_es_ES_Result.xml";
		try {
			merge.main(new String[] {invalidOutputArg, inputTemplateFolder, inputBaseFolder});
			fail();
		} catch (Exception e) {
			assertEquals(e.getClass(), InvalidFolderException.class);
		}
	}
	
	@Test
	public void templateMergeTemplateFilesFetch() {
		try {
			merge.main(new String[] {outputFolder, inputTemplateFolder, inputBaseFolder});
		} catch (Exception e) {
			fail();
		}
		assertEquals(1, merge.getTaskProcess().getTemplateFiles().length);
		assertEquals(1, merge.getTaskProcess().getBaseFiles().length);
		boolean foundTemplate = false;
		System.out.println(merge.getTaskProcess().getTemplateFiles().length);
		for (int i = 0; i < merge.getTaskProcess().getTemplateFiles().length; i++) {
			File file = merge.getTaskProcess().getTemplateFiles()[i];
			System.out.println(file.getName());
			if (file.getName().equals("AD_TASK_TRL_Template.xml")) foundTemplate = true;
		}
		assertTrue(foundTemplate);
	}
}
