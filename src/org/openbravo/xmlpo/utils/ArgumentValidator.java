package org.openbravo.xmlpo.utils;

import java.io.File;

import org.apache.log4j.Logger;

public class ArgumentValidator {
  
  public static final String INPUT_FOLDER = "Input folder";
  public static final String OUTPUT_FOLDER = "Output folder";
  public static final String PO_FILE = "PO file";
  public static final String XML_FILE = "XML file";
  public static final String IGNORE_FILE_STRING = "ignore";
  
  private static final Logger log4j = Logger.getLogger(ArgumentValidator.class);

  public static String validateFolder(String argType, String folderName) {
    String result = "";
    
    if (! new File(folderName).exists()) {
      result = argType + " (" + folderName + ") does not exist.";
    } else if (! new File(folderName).isDirectory()) {
      result = argType + " (" + folderName + ") is not a folder.";
    } 
    
    return result;
  }
  
  public static String validateFile(String argType, File fileName) {
    String result = "";
    
    log4j.debug("fileName: " + fileName.getName());
    
    
    if (!fileName.exists()) {
      result = argType + " (" + fileName + ") does not exist.";
    } else if (fileName.isDirectory()) {
      result = argType + " (" + fileName + ") is a folder, should be a file.";
    } else if ((fileName.getName()).toLowerCase().startsWith("contributors")) {
      result = IGNORE_FILE_STRING;
    }
    
    return result;
  }
}
