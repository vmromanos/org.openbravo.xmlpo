/* 
 * Copyright headissue GmbH, Jens Wilke, 2007 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;
import org.openbravo.xmlpo.objects.ValueType;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * Parses a value element and stores its data.
 * For each complete row rowComplete is called on the container obj.
 */
public class FileHandler extends DefaultHandler implements Cloneable  {
  
  private static final Logger log4j = Logger.getLogger(FileHandler.class);
  private FileObject file;
  private RowObject rowObj;
  private ValueObject valueObj;
  private elementType currentElement;
  private ValueType valueType = new ValueType();
  
  private enum elementType { COMPIERETRL, ROW, VALUE, NULL }
  
  public FileHandler() {
    file = new FileObject();
  }
  
  public FileHandler(FileObject fileObj) {
    log4j.debug("initiating FileHandler()");
    this.file = fileObj;
  }

  /**
   * Inherited from the DefaultHandler
   * 
   */
  public void startElement(String namespaceURI, String localName, String qName, Attributes attrs) throws SAXException {
    if (qName.equals("compiereTrl")) {
      currentElement = elementType.COMPIERETRL;
      file.setLanguage(attrs.getValue(FileObject.XMLFILE_LANGUAGE_ATTRNAME));
      file.setTableName(attrs.getValue(FileObject.XMLFILE_TABLE_ATTRNAME));
      file.setVersion(attrs.getValue(FileObject.XMLFILE_VERSION_ATTRNAME));
    } else if (qName.equals("row")) {
      currentElement = elementType.ROW;
      rowObj = new RowObject();
      rowObj.setRowId(attrs.getValue(RowObject.XMLROW_ROWID_ATTRNAME));
      rowObj.setTranslation(attrs.getValue(RowObject.XMLROW_TRL_ATTRNAME));
    } else if (qName.equals("value")) {
      currentElement = elementType.VALUE;
      valueObj = new ValueObject();
      valueObj.setColumn(attrs.getValue(ValueObject.XMLCOLUMN_COLUMN_ATTRNAME));
      valueObj.setOriginal(attrs.getValue(ValueObject.XMLCOLUMN_ORIGINAL_ATTRNAME).replaceAll("\r",""));
      valueObj.setValue("");
      valueType = new ValueType();
      valueType.setName(attrs.getValue("column"));
      valueType = file.addValueType(valueType);
      valueObj.setType(valueType);
    }
  }
  
  /**
   * Inherited from the DefaultHandler
   * 
   */
  public void characters(char[] chars, int start, int length) throws SAXException {
    if (currentElement.equals(elementType.VALUE)) {
      valueObj.appendDataValue(new String(chars, start, length).replaceAll("\r",""));
    }
  }
  
  /**
   * Inherited from the DefaultHandler
   * 
   */
  public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
    FileContainer container = file.getContainer();
    if (qName.equals("value")) {
      rowObj.addValueObject(valueObj);
    } else if (qName.equals("row")) {
      file.addRowObject(rowObj);
    } else if (qName.equals("compiereTrl")) {
      container.addFileObject(file);
      file.setContainer(container);
    }
    // set the currentElement to null to avoid unexpected calls to character method
    currentElement = elementType.NULL;
  }

  public FileObject getFileObj() { return file; }
}
