/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class StringFormatter {

	private static final Logger log4j = Logger.getLogger(StringFormatter.class);
	
	public static String stringEscapeFormat(String value, boolean addEscapeChar) {
		String result = new String();
		
		if (addEscapeChar) {
			result = addEscapeChars(value);
		} else {
			result = removeEsapeChars(value);
		}
		
		return result;
	}
	
	private static String addEscapeChars(String original) {
		String result = new String();
		String temp = original;
		
		// Check for escaped quotation strings - using Regex
		temp = matchAndReplace(temp, "(?<=\\\\)\"", "\\\\\\\\\"");
		// Check for unescaped quotation strings - using Regex 
		temp = matchAndReplace(temp, "(?<!\\\\)\"", "\\\\\"");
		
		result = temp;
		return result;
	}
	
	private static String removeEsapeChars(String original) {
		String result = new String();
		String temp = original;

		log4j.debug(original);
		// Check for quotation strings that should not be escaped - using Regex 
		temp = matchAndReplace(temp, "(?<!\\\\)\\\\\"", "\"");
		// Check for quotation strings that should be escaped - using Regex
		temp = matchAndReplace(temp, "\\\\\\\\\"", "\\\"");

		result = temp;
		return result;
	}

	private static String matchAndReplace(String inputString, String match, String replace) {
		log4j.debug("inputString: " + inputString);
		String result = inputString;
		Pattern pattern = Pattern.compile(match);
		CharSequence input = result;
		Matcher search = pattern.matcher(input);
		
		result = search.replaceAll(replace);
		
		return result;
	}
	
}
