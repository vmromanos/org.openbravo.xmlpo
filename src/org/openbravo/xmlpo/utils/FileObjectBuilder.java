/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.objects.POFileObject;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;
import org.openbravo.xmlpo.objects.ValueType;


public class FileObjectBuilder {
  
  private static final Logger log4j = Logger.getLogger(FileObjectBuilder.class);

  private FileObject fileObject;
  private POFileObject poFileObject;
  
  public FileObjectBuilder() {
    fileObject = new FileObject();
  }

  public FileObject getFileObject() { return fileObject; }
  
  public void setPOFileObject(POFileObject poFile) { poFileObject = poFile; }
  
  public void createFileObject() {
    fileObject = new FileObject();
    setTableName(poFileObject.getTableName());
    setLanguage(poFileObject.getLanguage());
    for (Iterator<ValueType> poFileIter = poFileObject.getValueTypes().iterator(); poFileIter.hasNext();) {
      ValueType type = (ValueType) poFileIter.next();
      log4j.debug("adding valueType to fileObject: " + type.getName());
      fileObject.addValueType(type);
    }
    
    createRowObjects();
  }

  private void setTableName(String tableName) { fileObject.setTableName(tableName); }
  private void setLanguage(String lang) { fileObject.setLanguage(lang); }
  
  private void createRowObjects() {
    HashMap<String, RowObject> map = new HashMap<String, RowObject>();
    for (Iterator<Message> iterator = poFileObject.getMessages().iterator(); iterator.hasNext();) {
      Message msg = (Message) iterator.next();
      ValueType valueType = checkValueType(msg.getColumnIndex(), msg.getColumn());
      RowObject row = new RowObject();
      row.setRowId(msg.getRowIdRow());
      if (map.containsKey(row.getRowId())) {
        row = map.get(row.getRowId());
      } else {
        map.put(row.getRowId(), row);
      }
      ValueObject valueObj = createValueObject(msg, valueType);
      if (row.getTranslation() == null) {
        row.setTranslation(msg.getRowTrl());
      } else if (!row.getTranslation()) {
        if (!msg.getRowTrl() || valueObj.getOriginal().equals(valueObj.getValue())) {
          row.setTranslation(false);
        } else {
          row.setTranslation(true);
        }
      }
      row.addValueObject(valueObj);
      
      map.put(row.getRowId(), row);
    }
    Collection<RowObject> rows = new ArrayList<RowObject>(map.values());
    for (Iterator<RowObject> iterator = rows.iterator(); iterator.hasNext();) {
      RowObject rowObject = (RowObject) iterator.next();
      fileObject.addRowObject(rowObject);
    }
  }
  
  private ValueObject createValueObject(Message msg, ValueType valueType) {
    ValueObject obj = new ValueObject();
    obj.setType(valueType);
    obj.setOriginal(msg.getMessageId());
    obj.setValue(msg.getMessageString());
    
    return obj;
  }
  
  private ValueType checkValueType(int columnIndex, String columnName) {
    ValueType type = new ValueType();
    type.setIntValue(columnIndex);
    type.setName(columnName);
    type = fileObject.addValueType(type);
    return type;
  }
}
