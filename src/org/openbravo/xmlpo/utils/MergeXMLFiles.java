/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import java.util.HashMap;
import java.util.Iterator;

import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;

public class MergeXMLFiles {
	
	private FileObject templateObj;
	private FileObject baseObj;
	private FileObject mergedObj;
	private HashMap<String, RowObject> baseMap;

	public MergeXMLFiles(FileObject template, FileObject base) {
		templateObj = template;
		baseObj = base;
	}
	
	public void mergeFiles() {
		createMergedObj();
		createBaseMap();
		createRowObjects();
	}
	
	public FileObject getMergedFileObject() { return mergedObj; }
	
	private void createMergedObj() {	
		mergedObj = new FileObject();
		mergedObj.setTableName(templateObj.getTableName());
		mergedObj.setLanguage(baseObj.getLanguage());
		mergedObj.setVersion(templateObj.getVersion());
	}
	
	private void createRowObjects() {
		for (Iterator<RowObject> iterator = templateObj.getRowObjects().iterator(); iterator.hasNext();) {
			RowObject rowTemplate = (RowObject) iterator.next();
			RowObject mergeRow = new RowObject();
			mergeRow = rowTemplate;
			RowObject baseRow = null;
			if (baseMap.containsKey(mergeRow.getRowId())) {
				baseRow = baseMap.get(mergeRow.getRowId());
			} else {
				mergeRow.setTranslation(false);
			}
			if (baseRow != null) {
				mergeRow = mergeRowObjects(mergeRow, baseRow);
			}
			mergedObj.addRowObject(mergeRow);
		}
	}
	
	private void createBaseMap() {
		baseMap = new HashMap<String, RowObject>();
		for (Iterator<RowObject> iterator = baseObj.getRowObjects().iterator(); iterator.hasNext();) {
			RowObject baseRow = (RowObject) iterator.next();
			baseMap.put(baseRow.getRowId(), baseRow);
		}
	}
	
	private RowObject mergeRowObjects(RowObject targetRow, RowObject baseRow) {
		for (Iterator<ValueObject> iterator = targetRow.getValueObjects().iterator(); iterator.hasNext();) {
			ValueObject targetValue = (ValueObject) iterator.next();
			for (Iterator<ValueObject> iterator2 = baseRow.getValueObjects().iterator(); iterator2
					.hasNext();) {
				ValueObject baseValue = (ValueObject) iterator2.next();
				if (targetValue.getColumn().equalsIgnoreCase(baseValue.getColumn())) {
					targetValue.setValue(setTargetValue(baseValue));
					if (!targetValue.getOriginal().equalsIgnoreCase(baseValue.getOriginal())) {
						targetRow.setTranslation(false);
					}
					if (targetValue.getValue() == null || targetValue.getValue().equals("")) {
						targetRow.setTranslation(false);
					}
				}
			}
		}
		return targetRow;
	}
	
	private String setTargetValue(ValueObject baseValue) {
		String result = "";
		if (baseValue.getValue() != null) {
			result = baseValue.getValue();
		}
		return result;
	}
}
