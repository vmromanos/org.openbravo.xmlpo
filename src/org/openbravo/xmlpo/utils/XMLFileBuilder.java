/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SL 
 * All portions are Copyright (C) 2001-2019 Openbravo SL 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/

package org.openbravo.xmlpo.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;
import org.openbravo.xmlpo.objects.ValueType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class XMLFileBuilder {
  
  private static final Logger log4j = Logger.getLogger(XMLFileBuilder.class);
  
  /** XML Element Tag     */
  public static final String  XML_TAG = "compiereTrl";
  /** XML Attribute Table     */
  public static final String  XML_ATTRIBUTE_TABLE = FileObject.XMLFILE_TABLE_ATTRNAME;//"table";
  /** XML Attribute Language    */
  public static final String  XML_ATTRIBUTE_LANGUAGE = FileObject.XMLFILE_LANGUAGE_ATTRNAME;//"language";
  /** XML Attribute Version     */
  public static final String  XML_ATTRIBUTE_Version = FileObject.XMLFILE_VERSION_ATTRNAME;//"version";

  /** XML Row Tag         */
  public static final String  XML_ROW_TAG = "row";
  /** XML Row Attribute ID    */
  public static final String  XML_ROW_ATTRIBUTE_ID = "id";
  /** XML Row Attribute Translated  */
  public static final String  XML_ROW_ATTRIBUTE_TRANSLATED = "trl";

  /** XML Value Tag       */
  public static final String  XML_VALUE_TAG = "value";
  /** XML Value Column      */
  public static final String  XML_VALUE_ATTRIBUTE_COLUMN = "column";
  /** XML Value Original      */
  public static final String  XML_VALUE_ATTRIBUTE_ORIGINAL = "original";
  
  public static final String CONTRIBUTORS_FILENAME = "CONTRIBUTORS";
  public static final String XML_CONTRIB = "Contributors";
  
  private FileObject fileObject;
  private Document document;
  private String outputFolder;
  HashMap<Integer, ValueObject> typeMap = new HashMap<Integer, ValueObject>();
  
  public XMLFileBuilder() {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = null;
    try {
      builder = factory.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      log4j.error(e.getMessage());
      e.printStackTrace();
    }
    document = builder.newDocument();
  }
  
  public void setFileObject(FileObject fileObj) { fileObject = fileObj; }
  
  public void build() {
    if (fileObject != null && fileObject.getTableName() != null && !fileObject.getTableName().equals("")) {
      generateXML();
    }
  }
  
  public void setOutputFolder(String outputFolder) { 
    this.outputFolder = outputFolder;
    log4j.debug("Output folder set: " + outputFolder);
  }
  
  private void generateXML() {
    String tableName = fileObject.getTableName() + "_TRL";
    String language = fileObject.getLanguage();
    
    String fileName = outputFolder + tableName + "_" + language + ".xml";
    File out = new File(fileName);
    log4j.debug("Generating xml for filename: " + fileName);
    initiateRowTemplate();
    
    // Root
    Element root = document.createElement(XML_TAG);
    root.setAttribute(XML_ATTRIBUTE_LANGUAGE, fileObject.getLanguage());
    root.setAttribute(XML_ATTRIBUTE_TABLE, fileObject.getTableName());
    root.setAttribute(XML_ATTRIBUTE_Version, fileObject.getVersion());
    document.appendChild(root);
    List<RowObject> rows = new ArrayList<RowObject>(fileObject.getRowObjects());
    Collections.sort(rows, new RowObject());
    for (Iterator<RowObject> iterator = rows.iterator(); iterator.hasNext();) {
      initiateRowTemplate();
      RowObject rowObject = (RowObject) iterator.next();
      Element row = document.createElement (XML_ROW_TAG);
      row.setAttribute(XML_ROW_ATTRIBUTE_ID, rowObject.getRowId()); //  KeyColumn
      log4j.debug("rowid: " + rowObject.getRowId() + " ,row translation: " + rowObject.getTranslation());
      String isTrl = parseTranslationString(rowObject.getTranslation());
      row.setAttribute(XML_ROW_ATTRIBUTE_TRANSLATED, isTrl);    //  IsTranslated
      
      for (Iterator<ValueObject> it = rowObject.getValueObjects().iterator(); it.hasNext();) {
        ValueObject vObj = (ValueObject) it.next();
        log4j.debug("pop typemap vObj: " + vObj.getType().getName());
        typeMap.put(vObj.getType().getIntValue(), vObj);
      }
      log4j.debug("typeMap size: " + typeMap.size());
      int typeCount = 1;
      while (typeCount <= typeMap.size()) {
        ValueObject obj = typeMap.get(typeCount);
        Element value = document.createElement (XML_VALUE_TAG);
        value.setAttribute(XML_VALUE_ATTRIBUTE_COLUMN, obj.getType().getName());
        log4j.debug("typeCount" + typeCount);
        String origString;
        if (obj.getOriginal() == null) {
          origString = "";
        } else {
          origString = StringFormatter.stringEscapeFormat(obj.getOriginal(), false);      //  Original Value
        }
        String valueString;
        if (obj.getValue() == null) {
          valueString = "";
        } else {
          valueString = StringFormatter.stringEscapeFormat(obj.getValue(), false);       //  Value
        }
        value.setAttribute(XML_VALUE_ATTRIBUTE_ORIGINAL, origString);
        value.appendChild(document.createTextNode(valueString));
        row.appendChild(value);
        typeCount++;
      }
      root.appendChild(row);
    }
    
    DOMSource source = new DOMSource(document);
    TransformerFactory tFactory = TransformerFactory.newInstance();
    tFactory.setAttribute("indent-number", Integer.valueOf(2));
    Transformer transformer;
    OutputStreamWriter osw;
    try {
      transformer = tFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      //  Output
      out.createNewFile();
      //  Transform
      osw = new OutputStreamWriter(new FileOutputStream (out));
      transformer.transform (source, new StreamResult(osw));
    } catch (TransformerConfigurationException e) {
      log4j.error(e.getMessage());
      e.printStackTrace();
    } catch (FileNotFoundException e) {
    	log4j.error(e.getMessage());
      e.printStackTrace();
    } catch (IOException e) {
    	log4j.error(e.getMessage());
      e.printStackTrace();
    } catch (TransformerException e) {
    	log4j.error(e.getMessage());
        e.printStackTrace();
    }
    log4j.info("Completed writing XML File: " + fileName);
  }
  
  private void initiateRowTemplate() {
    typeMap = new HashMap<Integer, ValueObject>();
    for (Iterator<ValueType> iterator = fileObject.getValueTypes().iterator(); iterator.hasNext();) {
      ValueType type = (ValueType) iterator.next();
      ValueObject obj = new ValueObject();
      obj.setType(type);
      obj.setColumn(type.getName());
      log4j.debug("column name: " + obj.getColumn());
      typeMap.put(type.getIntValue(), obj);
    }
  }
  
  private String parseTranslationString(boolean value) {
    String result = "N";
    if (value) result = "Y";
    return result;
  }
}
