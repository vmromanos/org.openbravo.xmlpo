/* 
 * Copyright headissue GmbH, Jens Wilke, 2007 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.exceptions.InvalidMergeException;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.objects.POFileObject;


public class POFileParser {

  private static final Logger log4j = Logger.getLogger(POFileParser.class);
  
  private MessageParser msgParser;
  private File file;
  private POFileObject poFileObj;
  private boolean fileMatch = true;
  private ParseType parserType;
  
  public enum ParseType { MERGE, TRANSFORM }
  
  public POFileParser() {
    poFileObj = new POFileObject();
  }
  
  public POFileParser(MessageParser parser) {
    this();
    setMessageParser(parser);
  }
  
  public void setParseType(ParseType type) { parserType = type; }
  public ParseType getParserType() { return parserType; }

  public File getFile() { return file; }
  public void setFile(File file) { this.file = file; }
  
  public void parseFile() throws InvalidMergeException {
    readFile();
  }
  
  public MessageParser getMessageParser() { return msgParser; }
  public void setMessageParser(MessageParser parser) {msgParser = parser; }
  
  public boolean getFileMatchResult() { return fileMatch; }
  
  public POFileObject getPOFileObject() { return poFileObj; }
  
  private void readFile() throws InvalidMergeException {
    try {
      if (file != null) {
        FileReader reader = new FileReader(file);
        LineNumberReader lineReader = new LineNumberReader(reader);
        String line = "";
        boolean noMoreLinesFlag = true;
        boolean processMsgs = false;
        while (noMoreLinesFlag && fileMatch) {
          line = lineReader.readLine();
          if (line == null) {
            noMoreLinesFlag = false;
          } else {
            if (line.startsWith(Message.MESSAGE_FLAG_STRING)) processMsgs = true;
            if (processMsgs) {
              msgParser.parseLine(line);
            } else {
              if (!isHeaderComplete()) {
                parseHeader(line);
              } else if (parserType.equals(ParseType.MERGE)) {
                validateFileMerge(line);
              }
            }
          }
        }
        lineReader.close();
      }
    } catch (FileNotFoundException e) {
      log4j.error("\n!!! ERROR: " + e.getMessage() + "\n" + "File: " + file.getName() + "\n");
    } catch (IOException e) {
      log4j.error("\n!!! ERROR: " + e.getMessage() + "\n" + "File: " + file.getName() + "\n");
    } catch (SecurityException e) {
      log4j.error(e.getMessage());
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      log4j.error(e.getMessage());
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      log4j.error(e.getMessage());
      e.printStackTrace();
    }
  }

  private boolean isHeaderComplete() {
    boolean result = true;
    result = isEmptyAttribute(poFileObj.getTableName());
    result = isEmptyAttribute(poFileObj.getFileName());
    result = isEmptyAttribute(poFileObj.getColumnName());
    result = isEmptyAttribute(poFileObj.getColumnIndex());
    result = isEmptyAttribute(poFileObj.getLanguage());
    result = isEmptyAttribute(poFileObj.getVersion());
    
    return result;
  }
  
  private boolean isEmptyAttribute(Object attribute) {
    boolean result = true;
    if (attribute == null || attribute.equals("")) result = false;
    return result;
  }
  
  private void parseHeader(String line) throws SecurityException, NoSuchMethodException, ClassNotFoundException {
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.FILENAME_STRING)) {
      poFileObj.setFileName(trimHeaderLine(line, POFileObject.FILENAME_STRING));
    }
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.TABLENAME_STRING)) {
      poFileObj.setTableName(trimHeaderLine(line, POFileObject.TABLENAME_STRING));
    }
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.COLUMNNAME_STRING)) {
      poFileObj.setColumnName(trimHeaderLine(line, POFileObject.COLUMNNAME_STRING));
      msgParser.setColumnName(poFileObj.getColumnName());
    }
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.COLUMNINDEX_STRING)) {
      poFileObj.setColumnIndex(trimHeaderLine(line, POFileObject.COLUMNINDEX_STRING));
      msgParser.setColumnIndex(poFileObj.getColumnIndex());
    }
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.LANGUAGE_STRING)) {
      poFileObj.setLanguage(trimHeaderLine(line, POFileObject.LANGUAGE_STRING));
    }
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.VERSION_STRING)) {
      poFileObj.setVersion(trimHeaderLine(line, POFileObject.VERSION_STRING));
    }
  }
  
  private void validateFileMerge(String line) throws InvalidMergeException {
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.FILENAME_STRING)) {
      if (!poFileObj.getFileName().equals(trimHeaderLine(line, POFileObject.FILENAME_STRING))) {
        fileMatch = false;
        String errorStr = "\nFILE: " + file.getName() + 
            "\nFirst Value: " + poFileObj.getFileName() + 
            ", Second Value: " + trimHeaderLine(line, POFileObject.FILENAME_STRING);
        throw new InvalidMergeException("PO '" + POFileObject.FILENAME_STRING + "' does not match." + errorStr);
      }
    }
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.TABLENAME_STRING)) {
      if (!poFileObj.getTableName().equals(trimHeaderLine(line, POFileObject.TABLENAME_STRING))) {
        fileMatch = false;
        String errorStr = "\nFILE: " + file.getName() + 
        "\nFirst Value: " + poFileObj.getTableName() + 
        ", Second Value: " + trimHeaderLine(line, POFileObject.TABLENAME_STRING);
        throw new InvalidMergeException("PO '" + POFileObject.TABLENAME_STRING + "' does not match." + errorStr);
      }
    }
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.COLUMNNAME_STRING)) {
      if (!poFileObj.getColumnName().equals(trimHeaderLine(line, POFileObject.COLUMNNAME_STRING)))  {
        fileMatch = false;
        String errorStr = "\nFILE: " + file.getName() + 
        "\nFirst Value: " + poFileObj.getColumnName() + 
        ", Second Value: " + trimHeaderLine(line, POFileObject.COLUMNNAME_STRING);
        throw new InvalidMergeException("PO '" + POFileObject.COLUMNNAME_STRING + "' does not match." + errorStr);
      }
    }
    if (line.startsWith(POFileObject.HEADER_IDENTIFIER + POFileObject.LANGUAGE_STRING)) {
      if (!poFileObj.getLanguage().equals(trimHeaderLine(line, POFileObject.LANGUAGE_STRING))) {
        fileMatch = false;
        String errorStr = "\nFILE: " + file.getName() + 
        "\nFirst Value: " + poFileObj.getLanguage() + 
        ", Second Value: " + trimHeaderLine(line, POFileObject.LANGUAGE_STRING);
        throw new InvalidMergeException("PO '" + POFileObject.LANGUAGE_STRING + "' does not match." + errorStr);
      }
    }
  }
  
  private String trimHeaderLine(String line, String variable) {
    String result = line.substring(1);
    result = result.replaceAll(variable, "");
    result = result.replaceAll("\\\\n\"", ""); // Easier to define it in the quotes due to regex escape patterns
    result = result.trim();
    return result;
  }
  
}
