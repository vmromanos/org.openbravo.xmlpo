/* 
 * Copyright (C) 2001-2019 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import java.util.HashMap;

import org.openbravo.xmlpo.merge.MessageMerge;
import org.openbravo.xmlpo.objects.Message;


public class MessageParser {
  
  //private static final Logger log4j = Logger.getLogger(MessageParser.class);
  
  private Message message;
  private HashMap<String, Message> messages;
  private int lineCounter = 0;
  private boolean multiLineMsgId = false;
  private boolean multiLineMsgStr = false;
  private String column;
  private int columnIndex;
  
  public MessageParser() {
    messages = new HashMap<String, Message>();
    message = new Message();
  }
  
  public void parseLine(String line) {
    if (line != null) {
      lineCounter++;
      if (line.startsWith(Message.MESSAGE_FLAG_STRING)) {
        message = new Message();
        message.setColumn(column);
        message.setColumnIndex(columnIndex);
        lineCounter = 1;
      } else if (line.startsWith(Message.COMMENT_FLAG_STRING + Message.ROWTRL_MESSAGE_STRING)) {
        extractRowTrl(line);
      } else if (line.startsWith(Message.COMMENT_FLAG_STRING)) {
        if (lineCounter == 2) {
          extractRowId(line);
        } else {
          addCommentLine(line);
        }
      } else if (line.startsWith(Message.MSGID_STRING) && message.getRowIdRow() != null) {
        String temp = extractMsgIdValue(line);
        if (temp.startsWith("\n")) {
          temp.replaceFirst("\n", "");
        }
        message.setMessageId(temp);
        multiLineMsgId = true;
      } else if (line.startsWith(Message.MSGSTR_STRING) && message.getRowIdRow() != null) {
        message.setMessageString(extractMsgIdValue(line));
        multiLineMsgId = false;
        multiLineMsgStr = true;
      } else if (line.trim().startsWith("\"") && multiLineMsgId) {
        if (!message.getMessageId().equals("") && !message.getMessageId().endsWith("\n")) {
          message.setMessageId(message.getMessageId() + "\n");
        }
        message.setMessageId(message.getMessageId() + extractMsgIdValue(line));
      } else if (line.trim().startsWith("\"") && multiLineMsgStr) {
        if (!message.getMessageString().equals("") && !message.getMessageString().endsWith("\n")) {
          message.setMessageString(message.getMessageString() + "\n");
        }
        message.setMessageString(message.getMessageString() + extractMsgIdValue(line));
      } else if (line.trim().equals("") && multiLineMsgStr) {
        multiLineMsgStr = false;
        // in theory this should mean that the message item is now complete....don't know of any other way to complete this section?
        updateMessageMap(message);
      }
    }
  }
  
  public Message getMessage() { return message; }
  public HashMap<String, Message> getMessages() { return messages; }
  public void setColumnName(String column) { this.column = column; }
  public void setColumnIndex(int index) { columnIndex = index; }
  
  private String extractMsgIdValue(String line) {
    String result = line;
    if (result.indexOf("\"") + 1 == result.lastIndexOf("\"")) {
      result = "";
    } else {
      result = result.substring(result.indexOf("\"") + 1, result.lastIndexOf("\""));
    }
    return result;
  }
  
  private void addCommentLine(String line) {
    String temp;
    if (message.getCommentString() != null) {
       temp = message.getCommentString() + line;
    } else {
      temp = line;
    }
    message.setComments(temp + Message.LINE_TERMINATOR_STRING);
  }
  
  private void extractRowTrl(String line) {
    String temp = line.replaceAll(Message.COMMENT_FLAG_STRING, "");
    temp = temp.replaceAll(Message.ROWTRL_MESSAGE_STRING, "");
    temp = temp.replaceAll(Message.LINE_TERMINATOR_STRING, "");
    //log4j.debug("extracted row trl: " + temp);
    message.setRowTrl(temp);
  }
   
  private void extractRowId(String line) {
    String temp = line.replaceAll(Message.COMMENT_FLAG_STRING, "");
    temp = temp.replaceAll(Message.LINE_TERMINATOR_STRING, "");
    message.setRowIdRow(temp);
  }
  
  private void updateMessageMap(Message msg) {
    if (msg.getRowIdRow() != null && !(msg.getRowIdRow().equals(""))) {
      if (msg.getMessageString().trim() != null) {// && !(msg.getMessageString().trim().equals(""))) {
        if (msg.getRowIdRow().indexOf(",") > 0) {
          String rowId = msg.getRowIdRow();
          String row = "";
          while (rowId.indexOf(",") > 0) {
            int index = rowId.indexOf(",");
            row = rowId.substring(0,index);
            rowId = rowId.substring(index + 1);
            createMessage(msg, row);
          }
          createMessage(msg, rowId);
        } else {
          createMessage(msg, msg.getRowIdRow());
        }
      }
    }
  }
  
  private void createMessage(Message msg, String rowId) {
    Message myMessage = new Message();
    myMessage.setColumn(msg.getColumn());
    myMessage.setColumnIndex(msg.getColumnIndex());
    myMessage.setComments(msg.getCommentString());
    myMessage.setMessageId(msg.getMessageId());
    myMessage.setMessageString(msg.getMessageString());
    myMessage.setRowIdRow(rowId);
    myMessage.setRowTrl(msg.getRowTrl());
    if (myMessage.getMessageString().equals("")) {
      myMessage.setRowTrl("false");
    } else if (!myMessage.getRowTrl() && !(myMessage.getMessageId().equals(myMessage.getMessageString()))) {
      myMessage.setRowTrl("true");
    }
    addMessage(myMessage);
  }
  
  private void addMessage(Message msg) {
    if (!messages.containsKey(msg.getRowIdRow())) {
      messages.put(msg.getRowIdRow(), msg);
    } else {
      Message dupeMessage = messages.get(msg.getRowIdRow());
      MessageMerge merge = new MessageMerge(dupeMessage, msg);
      merge.mergeMessages();
      messages.remove(merge.getMessage().getRowIdRow());
      messages.put(merge.getMessage().getRowIdRow(), merge.getMessage());
    }
  }
  
}
