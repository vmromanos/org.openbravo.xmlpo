/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

public class TaskProcess {

	private String inputTemplateFolder = "";
	private String inputBaseFolder = "";
	private String outputFolder = "";
	private File[] templateFiles;
	private File[] baseFiles;
	
	public String getInputTemplateFolder() { return inputTemplateFolder; }
	public void setInputTemplateFolder(String inputTemplateFolder) { this.inputTemplateFolder = inputTemplateFolder; }
	
	public String getInputBaseFolder() { return inputBaseFolder; }
	public void setInputBaseFolder(String inputBaseFolder) { this.inputBaseFolder = inputBaseFolder; }
	
	public String getOutputFolder() { return outputFolder; }
	public void setOutputFolder(String outputFolder) { this.outputFolder = outputFolder; }
	
	public File[] getTemplateFiles() { return templateFiles; }
	public void setTemplateFiles(File[] templateFiles) { 
		//System.out.println("setTemplateFiles() size: " + templateFiles.length);
		Collection<File> files = new ArrayList<File>();
		for (int i = 0; i < templateFiles.length; i++) {
			File file = templateFiles[i];
			if (file.getName().endsWith(".xml")) {
				//System.out.println(file.getName());
				files.add(file);
			}
		}
		this.templateFiles = new File[files.size() -1];
		this.templateFiles = files.toArray(this.templateFiles); 
	}
	
	public File[] getBaseFiles() { return baseFiles; }
	public void setBaseFiles(File[] baseFiles) {
		Collection<File> files = new ArrayList<File>();
		for (int i = 0; i < baseFiles.length; i++) {
			File file = baseFiles[i];
			if (file.getName().endsWith(".xml")) {
				files.add(file);
			}
		}
		this.baseFiles = new File[files.size() - 1]; 
		this.baseFiles = files.toArray(this.baseFiles); 
	}
}
