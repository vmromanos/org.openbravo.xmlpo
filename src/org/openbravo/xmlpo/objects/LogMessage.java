/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

public class LogMessage {

	private StringBuffer messageBuffer;
	
	public LogMessage() {
		messageBuffer = new StringBuffer();
	}
	
	public String getMessage() { return messageBuffer.toString(); }
	
	public void appendMessage(String message) {
		if (messageBuffer.length() != 0) {
			messageBuffer.append("\n");
		}
		messageBuffer.append(message);
	}
}
