/* 
 * Copyright (C) 2001-2019 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

public class POFileObject {
  
  private static final Logger log4j = Logger.getLogger(POFileObject.class);
  
  public static final String FILENAME_STRING = "Project-Id-Version:";
  public static final String TABLENAME_STRING = "X-Openbravo-Table:";
  public static final String COLUMNNAME_STRING = "X-Openbravo-Column:";
  public static final String COLUMNINDEX_STRING = "X-Openbravo-Index-Column:";
  public static final String LANGUAGE_STRING = "X-Openbravo-Language:";
  public static final String VERSION_STRING = "X-Openbravo-Version:";
  public static final String HEADER_IDENTIFIER = "\"";
  public static final String HEADER_LINE_TERMINATOR = "\n\"";
  public static final String NEW_LINE_CHAR = "\n";

  private String fileName;
  private String tableName;
  private String columnName;
  private int columnIndex;
  private String language;
  private String version;
  private Collection<Message> messages = new ArrayList<Message>(); 
  private ValueType type = new ValueType();
  private Collection<ValueType> types = new ArrayList<ValueType>();
  private int valueInt = 1;
  
  public POFileObject() {
    messages = new ArrayList<Message>();
  }
  
  public String getFileName() { return fileName; }
  public void setFileName(String fileName) { this.fileName = fileName; }
  
  public String getTableName() { return tableName; }
  public void setTableName(String tableName) { this.tableName = tableName; }
  
  public String getColumnName() { return columnName; }
  public void setColumnName(String columnName) { 
    this.columnName = columnName; 
    type.setName(columnName);
    log4j.debug("type name set: " + type.getName());
  }
  
  public int getColumnIndex() { return columnIndex; }
  public void setColumnIndex(String index) { 
    columnIndex = Integer.valueOf(index);
    type.setIntValue(columnIndex);
  }
  
  public String getLanguage() { return language; }
  public void setLanguage(String language) { this.language = language; }
  
  public String getVersion() { return version; }
  public void setVersion(String obVersionNumber) { version = obVersionNumber; }
  
  public Collection<Message> getMessages() {return messages; }
  public void setMessages(Collection<Message> msgs) { messages = msgs; }
  public void addMessages(Collection<Message> msgs) {
    for (Iterator<Message> iterator = msgs.iterator(); iterator.hasNext();) {
      Message msg = (Message) iterator.next();
      messages.add(msg); 
    }
  }
  
  public ValueType getValueType() { return type; }
  public void setValueType(ValueType valueType) { 
    type = valueType;
  } 
  public Collection<ValueType> getValueTypes() { return types; }
  public ValueType addValueType(ValueType valueType) {
    if (types.size() > 0) {
      valueInt++;
      boolean matched = false;
      for (Iterator<ValueType> iterator = types.iterator(); iterator.hasNext();) {
          ValueType myType = (ValueType) iterator.next();
          if (myType.getName().equals(valueType.getName())) {
            matched = true;
            valueType.setIntValue(myType.getIntValue());
          }
      }
      if (!matched) {
        if (valueType.getIntValue() == 0) valueType.setIntValue(valueInt);
        types.add(valueType); 
      }
    } else {
      if (valueType.getIntValue() == 0) valueType.setIntValue(valueInt);
      types.add(valueType);
    }
    return valueType;
  } 
}
