/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class RowObject implements Comparator<RowObject>{
  
  public static final String XMLROW_ROWID_ATTRNAME = "id";
  public static final String XMLROW_TRL_ATTRNAME = "trl";
 
  private String rowId;
  private Boolean translation;
  private Collection<ValueObject> valueObjects;
  
  public RowObject() {
    valueObjects = new ArrayList<ValueObject>();
  }
  
  public String getRowId() { return rowId; }
  public void setRowId(String rowId) { this.rowId = rowId; }
  
  public Boolean getTranslation() { return translation; }
  public void setTranslation(String translation) {
    if (translation.toUpperCase().equals("Y")) {
      this.translation = true;
    } else if (translation.toUpperCase().equals("N")) {
      this.translation = false;
    } else {
      //TODO handle an invalid string
      this.translation = false;
    }
  }
  public void setTranslation(boolean rowTrl) { translation = rowTrl; }
   
  public Collection<ValueObject> getValueObjects() { return valueObjects; }
  public void setValueObjects(Collection<ValueObject> valueObjects) { this.valueObjects = valueObjects; }
  public void addValueObject(ValueObject object) { valueObjects.add(object); }

  public int compare(RowObject o1, RowObject o2) {
    String int1 = o1.getRowId();
    String int2 = o2.getRowId();
    
    int result;
    result = int1.compareTo(int2);
    return result;
  }
  
}
