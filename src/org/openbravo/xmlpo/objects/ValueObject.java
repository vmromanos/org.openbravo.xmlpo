/* 
 * Copyright headissue GmbH, Jens Wilke, 2007 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import java.util.Comparator;

public class ValueObject implements Comparator<ValueObject> {
  
  public static final String XMLCOLUMN_COLUMN_ATTRNAME = "column";
  public static final String XMLCOLUMN_ORIGINAL_ATTRNAME = "original";
  
  private String column;
  private String original;
  private String value;
  private ValueType type = new ValueType();
  
  public String getColumn() { return column; }
  public void setColumn(String column) { this.column = column; }
  
  public String getOriginal() { return original; }
  public void setOriginal(String original) { this.original = original; }
  
  public String getValue() { return value; }
  public void setValue(String value) { this.value = value; }
  public void appendDataValue(String dataValue) {
    String temp = getValue() + dataValue;
    setValue(temp);
  }
  
  public ValueType getType() { return type; }
  public void setType(ValueType type) { this.type = type; }
  
  public int compare(ValueObject o1, ValueObject o2) {
    Integer int1 = Integer.valueOf(o1.getType().getIntValue());
    Integer int2 = Integer.valueOf(o2.getType().getIntValue());
    
    int result;
    result = int1.compareTo(int2);
    return result;
  }
}
