/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.openbravo.xmlpo.utils.FileContainer;


public class FileObject {
  
  public static final String XMLFILE_LANGUAGE_ATTRNAME = "language";
  public static final String XMLFILE_TABLE_ATTRNAME = "table";
  public static final String XMLFILE_VERSION_ATTRNAME = "version";
  
  
  private String language;
  private String tableName;
  private String version = "";
  private Collection<RowObject> rowObjects = new ArrayList<RowObject>();
  private Collection<ValueType> types = new ArrayList<ValueType>();
  private int valueInt = 1;
  private FileContainer container;
  
  public String getLanguage() { return language; }
  public void setLanguage(String lang) { language = lang; }
  
  public String getTableName() { return tableName; }
  public void setTableName(String tableName) {this.tableName = tableName; }
  
  public String getVersion() { return version; }
  public void setVersion(String versionNumber) { version = versionNumber; }
  public Collection<RowObject> getRowObjects() { return rowObjects; }
  public void addRowObject(RowObject rowObject) { rowObjects.add(rowObject); }
  
  public Collection<ValueType> getValueTypes() { return types; }
  
  public ValueType addValueType(ValueType valueType) {
    if (types.size() > 0) {
      valueInt++;
      boolean matched = false;
      for (Iterator<ValueType> iterator = types.iterator(); iterator.hasNext();) {
          ValueType type = (ValueType) iterator.next();
          if (type.getName().equals(valueType.getName())) {
            matched = true;
            valueType.setIntValue(type.getIntValue());
          }
      }
      if (!matched) {
        if (valueType.getIntValue() == 0) valueType.setIntValue(valueInt);
        types.add(valueType); 
      }
    } else {
      if (valueType.getIntValue() == 0) valueType.setIntValue(valueInt);
      types.add(valueType);
    }
    return valueType;
  } 
  
  public FileContainer getContainer() { 
	  return container; 
  }

  public void setContainer(FileContainer cont) {
	  container = cont; 
  }
  
}
