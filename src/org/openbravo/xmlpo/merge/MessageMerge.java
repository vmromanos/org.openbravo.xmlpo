/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.merge;

import org.openbravo.xmlpo.objects.Message;

public class MessageMerge {
  
  private static final String DUPE_START_TAG = "<<<";
  private static final String DUPE_END_TAG = ">>>";
  private Message firstMsg;
  private Message secondMsg;
  private Message finalMessage;
  private boolean isDuplicate = true;
  
  public MessageMerge() {
    finalMessage = new Message();
  }
  
  public MessageMerge(Message first, Message second) {
    this();
    firstMsg = first;
    secondMsg = second;
  }
  
  public Message getMessage() { return finalMessage; }
  public void setFirstMessage(Message first) { firstMsg = first; }
  public void setSecondMessage(Message second) { secondMsg = second; }

  public boolean getDuplicateFlag() { return isDuplicate; }
  public void mergeMessages() { 
    if (firstMsg.getMessageId().equals(secondMsg.getMessageId()) && firstMsg.getMessageString().equals(secondMsg.getMessageString())) {
      isDuplicate = false;
    }
    finalMessage.setRowIdRow(firstMsg.getRowIdRow());
    finalMessage.setMessageId(mergeStrings(firstMsg.getMessageId(), secondMsg.getMessageId()));
    finalMessage.setMessageString(mergeStrings(firstMsg.getMessageString(), secondMsg.getMessageString()));
    finalMessage.setComments(mergeComments());
  }
  
  private String mergeComments() {
    String firstComment = firstMsg.getCommentString().substring(0, firstMsg.getCommentString().lastIndexOf("\n"));
    String secondComment = secondMsg.getCommentString().substring(0, secondMsg.getCommentString().lastIndexOf("\n"));
    
    StringBuffer buffer = new StringBuffer();
    buffer.append(firstComment);
    buffer.append(Message.LINE_TERMINATOR_STRING);
    if (isDuplicate) {
      buffer.append(secondComment);
      buffer.append(Message.LINE_TERMINATOR_STRING);
      buffer.append(Message.COMMENT_FLAG_STRING);
      buffer.append("DUPLICATION");
      buffer.append(Message.LINE_TERMINATOR_STRING);
    }
    return buffer.toString();
  }
  
  private String mergeStrings(String first, String second) {
    String result = "";
    if (isDuplicate && !(first.equals(second))) {
      String firstMsgId = DUPE_START_TAG + first + DUPE_END_TAG;
      String secondMsgId = DUPE_START_TAG + second + DUPE_END_TAG;
      result = firstMsgId + secondMsgId;
    } else {
      result = first;
    }
    return result;
  }
  
}
