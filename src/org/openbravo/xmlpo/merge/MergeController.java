/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.merge;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.exceptions.InvalidMergeException;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.utils.MessageParser;
import org.openbravo.xmlpo.utils.POFileBuilder;
import org.openbravo.xmlpo.utils.POFileParser;
import org.openbravo.xmlpo.utils.POFileParser.ParseType;


public class MergeController {

  private static final Logger log4j = Logger.getLogger(MergeController.class);
  
  private Collection<File> files = new ArrayList<File>();
  private POFileParser fileParser;
  private MessageParser msgParser; 
  private HashMap<String, Message> messages;
  private boolean validFiles;
  private String outputFolder = "";
  
  public MergeController() {
    msgParser = new MessageParser();
    fileParser = new POFileParser(msgParser);
  }
  
  public void processMerges() throws InvalidMergeException {
    for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
      File file = (File) iterator.next();
      readFiles(file);
      if (outputFolder == null || outputFolder.equals("")) {
        outputFolder = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf("/") + 1);
      }
    }
    if (isValidMerge()) {
      writePOFile();
    } else {
      log4j.error("!!!ERROR: invalid merge specified for these files.");
    }
  }
  
  public Collection<File> getFiles() { return files; }
  public void addFile(File inFile) {
    if (!(inFile == null) && !inFile.getName().equals("")) {
      files.add(inFile);
    }
  }
  
  public void setOutputFolder(String outFolder) {
    outputFolder = outFolder;
  }
  
  public void readFiles(File file) throws InvalidMergeException {
    fileParser.setFile(file);
    fileParser.setParseType(ParseType.MERGE);
    log4j.info("Merging file: " + file.getAbsolutePath());
    fileParser.parseFile();
    msgParser = fileParser.getMessageParser();
    messages = msgParser.getMessages();
    validFiles = fileParser.getFileMatchResult();
  }

  public HashMap<String, Message> getMessages() { return messages; }
  public boolean isValidMerge() { return validFiles; }
  
  public void writePOFile() {
    POFileBuilder builder = new POFileBuilder(true);
    String outputString = builder.generateFileContent(fileParser.getPOFileObject(), messages);
    String file = outputFolder + builder.getFileName();
    log4j.info("outputting file to : " + file + "");
    FileWriter fileWriter;
    try {
      fileWriter = new FileWriter(file);
      fileWriter.write(outputString);
      fileWriter.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
}