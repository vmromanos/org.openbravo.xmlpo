/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.merge;

import java.io.File;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.exceptions.InvalidFolderException;
import org.openbravo.xmlpo.exceptions.InvalidMergeException;
import org.openbravo.xmlpo.utils.ArgumentValidator;


public class POMerge {
  
  private static final Logger log4j = Logger.getLogger(POMerge.class);
  
  private static String outputFolder = "";
  private static File[] files;

  public static void main(String[] args) throws Exception {
    if (args.length > 0) {
      parseArgs(args);
      MergeController controller = new MergeController();
      if (outputFolder != "") controller.setOutputFolder(outputFolder);
      for (int i = 0; i < files.length; i++) {
        File file = files[i];
        if (!file.exists()) throw new InvalidFolderException("File (" + files[i] + ") does not exist.");
        controller.addFile(file);
      }
      try {
        controller.processMerges();
      } catch (InvalidMergeException m) {
        log4j.error("\n!!! ERROR: " + m.getMessage() + "\n");// + "File: " + file.getName() + "\n");
      }
    } else {
      log4j.info("Insufficient arguments.");
    }
    
  }
  
  private static void parseArgs(String[] args) throws InvalidFolderException {
    if (args[0] != null && !(args[0].equals(""))) {
      outputFolder = args[0];
      if (!outputFolder.endsWith("/")) outputFolder = outputFolder + "/";
      log4j.debug("output folder: " + outputFolder);
      String isValid = ArgumentValidator.validateFolder(ArgumentValidator.OUTPUT_FOLDER, outputFolder);
      if (!isValid.equals("")) throw new InvalidFolderException(isValid);
    }
    files = new File[args.length - 1];
    for (int i = 1; i < args.length; i++) {
      files[i-1] = new File(args[i]);
      log4j.debug("file being added from args: " + args[i]);
    }
  }
}


