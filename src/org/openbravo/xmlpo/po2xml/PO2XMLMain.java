/* 
 * Copyright headissue GmbH, Jens Wilke, 2007 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.po2xml;

import java.io.File;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.exceptions.InvalidFolderException;
import org.openbravo.xmlpo.utils.ArgumentValidator;

public class PO2XMLMain {
  
  private static final Logger log4j = Logger.getLogger(PO2XMLMain.class);
  
  private static String inputFolder = "";
  private static String outputFolder = "";
  private static File[] files;
  
  /**
   * @param args
   */
  public static void main(String[] args) throws Exception {
    parseArgs(args);
    if (args.length > 0) {
      PO2XMLController controller = new PO2XMLController();
      controller.setOutputFolder(outputFolder);
      for (int i = 0; i < files.length; i++) {
        File file = files[i];
        if (file.getName().endsWith(".po")) {
          String isValid = ArgumentValidator.validateFile(ArgumentValidator.PO_FILE, file);
          if (!isValid.equals("")) throw new InvalidFolderException(isValid);
          controller.addFile(file);
        }
        log4j.info("Adding file to transform to XML: " + file.getAbsoluteFile());
      }
      controller.readFiles();
      controller.buildFileObjects();
      controller.buildXMLFiles();
    } else {
      log4j.info("Insufficient arguments.");
      throw new InvalidFolderException("Insufficient arguments");
    }
  }
  
  private static void parseArgs(String[] args) throws InvalidFolderException {
    if (args[0] != null && !(args[0].equals(""))) {
      outputFolder = args[0];
      if (!outputFolder.endsWith("/")) outputFolder = outputFolder + "/";
      log4j.info("output folder: " + outputFolder);
      String isValid = ArgumentValidator.validateFolder(ArgumentValidator.OUTPUT_FOLDER, outputFolder);
      if (!isValid.equals("")) throw new InvalidFolderException(isValid);
    }
    if (args[1] != null && !(args[1].equals(""))) {
      inputFolder = args[1];
      if (!inputFolder.endsWith("/")) inputFolder = inputFolder + "/";
      log4j.info("inputFolder: " + inputFolder);
      String isValid = ArgumentValidator.validateFolder(ArgumentValidator.INPUT_FOLDER, inputFolder);
      if (!isValid.equals("")) throw new InvalidFolderException(isValid);
    }
    if (args.length < 3) {
      if (!inputFolder.equals("")) {
        File file = new File(inputFolder);
        if (file.isDirectory()) {
          files = file.listFiles();
          log4j.debug("files being generated from input folder: " + files.length);
        }
      }
    } else {
      files = new File[args.length - 2];
      for (int i = 2; i < args.length; i++) {
        files[i-2] = new File(inputFolder + args[i]);
        log4j.debug("file being added from args: " + args[i]);
      }
    }
  }
}
