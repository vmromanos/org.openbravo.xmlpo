/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.po2xml;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.exceptions.InvalidMergeException;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.objects.POFileObject;
import org.openbravo.xmlpo.utils.FileObjectBuilder;
import org.openbravo.xmlpo.utils.MessageParser;
import org.openbravo.xmlpo.utils.POFileParser;
import org.openbravo.xmlpo.utils.XMLFileBuilder;
import org.openbravo.xmlpo.utils.POFileParser.ParseType;


public class PO2XMLController {
  
  private static final Logger log4j = Logger.getLogger(PO2XMLController.class);

  private Collection<File> files;
  private Collection<POFileObject> poFiles;
  private Collection<FileObject> fileObjects;
  private String outputFolder;
  
  public PO2XMLController() {
    files = new ArrayList<File>();
    poFiles = new ArrayList<POFileObject>();
    fileObjects = new ArrayList<FileObject>();
  }
  
  public Collection<File> getFiles() { return files; }
  public void addFile(File newFile) { files.add(newFile); }
  
  public void setOutputFolder(String path) { outputFolder = path; }
  
  public Collection<POFileObject> getPOFilesObjects() { return poFiles; }
  
  public void readFiles() throws InvalidMergeException {
    for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
      File poFile = (File) iterator.next();
      log4j.debug("about to read file : " + poFile.getName());
      readFile(poFile);
    }
  }
  
  public void buildFileObjects() {
    FileObjectBuilder builder = new FileObjectBuilder();
    for (Iterator<POFileObject> iterator = poFiles.iterator(); iterator.hasNext();) {
      POFileObject poFile = (POFileObject) iterator.next();
      log4j.debug("poFile: " + poFile.getTableName());
      builder.setPOFileObject(poFile);
      builder.createFileObject();
      fileObjects.add(builder.getFileObject());
    }
    log4j.info("Completed generating FileObjects for XML Output. Total File Objects: " + fileObjects.size());
  }

  public void buildXMLFiles() {
    log4j.debug("fileObjects size: " + fileObjects.size());
    if (fileObjects.size() > 0) {
      for (Iterator<FileObject> iterator = fileObjects.iterator(); iterator.hasNext();) {
        FileObject fileObj = (FileObject) iterator.next();
        log4j.debug("Building file for File: " + fileObj.getTableName() + ", Language: " + fileObj.getLanguage() + ", typeSize: " + fileObj.getValueTypes().size());
        buildXMLFile(fileObj);
      }
    }
  }
  
  private void readFile(File file) throws InvalidMergeException {
    POFileParser parser = new POFileParser();
    MessageParser msgParser = new MessageParser(); 
    parser.setMessageParser(msgParser);
    parser.setParseType(ParseType.TRANSFORM);
    parser.setFile(file);
    parser.parseFile();
    POFileObject poFileObj = parser.getPOFileObject();
    Collection<Message> msgs = new ArrayList<Message>((msgParser.getMessages()).values());
    poFileObj.setMessages(msgs);
    poFileObj.addValueType(poFileObj.getValueType());
    log4j.debug("parser file object type name: " + poFileObj.getValueType().getName());
    if (poFiles.size() > 0) {
      addPOFileObject(poFileObj);
    } else {
      poFiles.add(poFileObj);
    }
  }
  
  private void addPOFileObject(POFileObject poFileObj) {
    boolean newPOFile = true;
    log4j.debug("addPOFileObject: " + poFileObj.getFileName());
    if (poFiles.size() > 0) {
    	for (Iterator<POFileObject> iterator = poFiles.iterator(); iterator.hasNext();) {
	      POFileObject poObj = (POFileObject) iterator.next();
	      if (existsFileObj(poObj, poFileObj)) {
	        newPOFile = false;
	        poObj.addValueType(poFileObj.getValueType());
	        poObj.addMessages(poFileObj.getMessages());
	      } /*else {
	        poFiles.add(fileObj);
	      }*/
	    }
    }
    if (newPOFile) poFiles.add(poFileObj);
  }
  
  private boolean existsFileObj(POFileObject base, POFileObject newFile) {
    boolean result = false;
    String tblName = newFile.getTableName();
    String lang = newFile.getLanguage();
    if (base.getTableName().equals(tblName) && base.getLanguage().equals(lang)) result = true;
    //TODO need to implement action for duplicate column
    return result;
  }
  
  private void buildXMLFile(FileObject fileObject) {
    XMLFileBuilder builder = new XMLFileBuilder();
    log4j.debug("buildXMLFile for FileObj: " + fileObject.getTableName());
    builder.setFileObject(fileObject);
    builder.setOutputFolder(outputFolder);
    builder.build();
  }

}
