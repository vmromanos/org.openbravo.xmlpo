package org.openbravo.xmlpo.exceptions;

public class InvalidFolderException extends Exception {
  
  private static final long serialVersionUID = 1L;

  public InvalidFolderException() {
    super();
  }
  
  public InvalidFolderException(String exception) {
    super(exception);
  }
  
}
