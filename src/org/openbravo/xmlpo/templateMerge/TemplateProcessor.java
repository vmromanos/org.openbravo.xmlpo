/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.templateMerge;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.TaskProcess;
import org.openbravo.xmlpo.utils.FileHandler;
import org.openbravo.xmlpo.utils.MergeXMLFiles;
import org.openbravo.xmlpo.utils.ParserUtils;
import org.openbravo.xmlpo.utils.XMLFileBuilder;
import org.xml.sax.SAXException;

public class TemplateProcessor {
	
	private TaskProcess taskProc;
	private File[] templateFiles;
	private File[] baseFiles;
	private HashMap<String, FileObject> templateMap = new HashMap<String, FileObject>();
	private HashMap<String, FileObject> baseMap = new HashMap<String, FileObject>();
	private Collection<FileObject> mergedFileObjects = new ArrayList<FileObject>();
	
	public TemplateProcessor(TaskProcess taskProcess) {
		taskProc = taskProcess;
	}
	
	public void processFiles() {
		templateFiles = taskProc.getTemplateFiles();
		baseFiles = taskProc.getBaseFiles();
		parseXMLFiles();
		mergeBaseToTemplate();
	}
	
	public void buildXMLOutput() {
		XMLFileBuilder builder = new XMLFileBuilder();
		builder.setOutputFolder(taskProc.getOutputFolder());
		for (Iterator<FileObject> iterator = mergedFileObjects.iterator(); iterator.hasNext();) {
			FileObject fileObj = (FileObject) iterator.next();
			builder.setFileObject(fileObj);
			builder.build();
		}
	}
	
	public Collection<FileObject> getMergedFileObjects() { return mergedFileObjects; }
	
	public TaskProcess getTaskProcess() { return taskProc; }
	
	public HashMap<String, FileObject> getTemplateMap() { return templateMap; }
	public void setTemplateMap(HashMap<String, FileObject> templateMap) { this.templateMap = templateMap; }

	public HashMap<String, FileObject> getBaseMap() { return baseMap; }
	public void setBaseMap(HashMap<String, FileObject> baseMap) { this.baseMap = baseMap; }

	private void parseXMLFiles() {
		try {
			parseFiles(templateFiles, templateMap);
			parseFiles(baseFiles, baseMap);
		} catch (ParserConfigurationException e) {
			System.out.println(e.getMessage());
		} catch (SAXException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void parseFiles(File[] files, HashMap<String, FileObject> fileMap) throws ParserConfigurationException, SAXException, IOException {
		SAXParser parser = ParserUtils.instantiateParser();
		FileObject fileObj = null;
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			fileObj = new FileObject();
			TemplateContainer container = new TemplateContainer();
			fileObj.setContainer(container);
			FileHandler handler = new FileHandler(fileObj);
			parser.parse(file, handler);
			fileObj = handler.getFileObj();
			fileMap.put(fileObj.getTableName(), fileObj);
		}
	}
	
	private void mergeBaseToTemplate() {
		Collection<FileObject> templates = templateMap.values();
		MergeXMLFiles merger;
		for (Iterator<FileObject> iterator = templates.iterator(); iterator.hasNext();) {
			FileObject templateFileObject = (FileObject) iterator.next();
			merger = new MergeXMLFiles(templateFileObject, baseMap.get(templateFileObject.getTableName()));
			if (baseMap.containsKey(templateFileObject.getTableName())) {
				merger.mergeFiles();
			}
			mergedFileObjects.add(merger.getMergedFileObject());
 		}
	}
	
}
