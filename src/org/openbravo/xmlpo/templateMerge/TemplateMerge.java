/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.templateMerge;

import java.io.File;

import org.openbravo.xmlpo.exceptions.InvalidFolderException;
import org.openbravo.xmlpo.objects.LogMessage;
import org.openbravo.xmlpo.objects.TaskProcess;
import org.openbravo.xmlpo.utils.ArgumentValidator;


public class TemplateMerge {
	
	//private static final Logger log4j = Logger.getLogger(TemplateMerge.class);

	private static String outputFolder = "";
	private static String inputTemplateFolder = "";
	private static String inputBaseFolder = "";
	private static TaskProcess taskProcess;
	private static boolean result;
	  
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		taskProcess = new TaskProcess();
		if (args.length > 0) {
			System.out.println("running TemplateMerge class");
			if (args.length == 3) {
				parseArgs(args);
			}
			TemplateProcessor processor = new TemplateProcessor(taskProcess);
			processor.processFiles();
			processor.buildXMLOutput();
			System.out.println("completed TemplateMerge");
		} else {
		    System.out.println("Insufficient arguments.");
		}

	}
	
	public TaskProcess getTaskProcess() { return taskProcess; }
	public boolean getResult() { return result; }

	private static void parseArgs(String[] args) throws InvalidFolderException {
		LogMessage mesg = new LogMessage();
	    if (args[0] != null && !(args[0].equals(""))) {
	      outputFolder = args[0];
	      if (!outputFolder.endsWith("/")) outputFolder = outputFolder + "/";
	      mesg.appendMessage("output folder: " + outputFolder);
	      String isValid = ArgumentValidator.validateFolder(ArgumentValidator.OUTPUT_FOLDER, outputFolder);
	      if (!isValid.equals("")) throw new InvalidFolderException(isValid);
	      taskProcess.setOutputFolder(outputFolder);
	    }
	    if (args[1] != null && !(args[1].equals(""))) {
	      inputTemplateFolder = args[1];
	      if (!inputTemplateFolder.endsWith("/")) inputTemplateFolder = inputTemplateFolder + "/";
	      mesg.appendMessage("input template folder: " + inputTemplateFolder);
	      String isValid = ArgumentValidator.validateFolder(ArgumentValidator.INPUT_FOLDER, inputTemplateFolder);
	      if (!isValid.equals("")) throw new InvalidFolderException(isValid);
	      taskProcess.setInputTemplateFolder(inputTemplateFolder);
	      taskProcess.setTemplateFiles(new File(inputTemplateFolder).listFiles());
	    }
	    if (args[2] != null && !(args[2].equals(""))) {
	      inputBaseFolder = args[2];
	      if (!inputBaseFolder.endsWith("/")) inputBaseFolder = inputBaseFolder + "/";
	      mesg.appendMessage("input folder: " + inputBaseFolder);
	      String isValid = ArgumentValidator.validateFolder(ArgumentValidator.INPUT_FOLDER, inputBaseFolder);
	      if (!isValid.equals("")) throw new InvalidFolderException(isValid);
	      taskProcess.setInputBaseFolder(inputBaseFolder);
	      taskProcess.setBaseFiles(new File(inputBaseFolder).listFiles());
	    }
	    System.out.println(mesg.getMessage());
	}
}
